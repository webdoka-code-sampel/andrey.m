"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const elements = {
        header: document.querySelector(".header"),
        headerMenu: document.querySelector(".header-menu"),
        headerMenuBtn: document.querySelector(".header-menu__btn"),
        modalCallback: document.querySelector(".modal-callback"),
        callbackForm: document.querySelectorAll(".callback-form"),
        callbackBtnShow: document.querySelectorAll(".callback-btn"),
        callbackBtnClose: document.querySelector(".callback-form__close"),

        // Вычисляем ширину скроллбара
        scrollbarWidth:
            window.innerWidth - document.documentElement.clientWidth,
    };

    function menuShow() {
        const { headerMenu, headerMenuBtn } = elements;

        if (headerMenuBtn !== null) {
            headerMenuBtn.addEventListener("click", () => {
                headerMenu.classList.toggle("active");
            });
        }

        document.addEventListener("click", (e) => {
            if (!e.target.closest(".header-menu")) {
                headerMenu.classList.remove("active");
            }
        });
    }

    menuShow();

    function modalCallback() {
        const {
            modalCallback,
            callbackBtnShow,
            scrollbarWidth,
            header,
            callbackBtnClose,
        } = elements;

        function modalCallbackClose() {
            modalCallback.classList.remove("show");
            document.body.style.overflow = null;
            document.body.style.marginRight = null;
            header.style.paddingRight = null;
        }

        callbackBtnShow.forEach((btn) => {
            btn.addEventListener("click", () => {
                modalCallback.classList.add("show");
                document.body.style.overflow = "hidden";
                document.body.style.marginRight = scrollbarWidth + "px";
                header.style.paddingRight = scrollbarWidth + "px";
            });
        });

        callbackBtnClose.addEventListener("click", () => {
            modalCallbackClose();
        });

        document.addEventListener("click", (e) => {
            if (e.target.classList.contains("modal-callback__wrap")) {
                modalCallbackClose();
            }
        });
    }

    modalCallback();

    function callbackFormActions() {
        const { callbackForm } = elements;

        callbackForm.forEach((form) => {
            if (form !== null) {
                const callbackCheckbox = form.querySelector(
                    ".callback-form__check"
                );
                const callbackCheckboxInput = form.querySelector(
                    ".callback-form__check-input"
                );
                const loadList = form.querySelector(
                    ".callback-form__load-list"
                );

                const loadDelBtn = form.querySelector(
                    ".callback-form__load-del"
                );
                const loadInput = form.querySelector(
                    ".callback-form__load-input"
                );
                const loadBtn = form.querySelector(".callback-form__load-btn");
                const loadLabel = form.querySelector(
                    ".callback-form__load-label"
                );
                const contactBox = form.querySelector(
                    ".callback-form__contact-box"
                );
                const contactItem = contactBox.querySelectorAll(
                    ".callback-form__contact-item"
                );
                const contactInput = contactBox.querySelector(
                    ".callback-form__item-input"
                );
                const submitBtn = form.querySelector(".callback-form__submit");

                callbackCheckbox.addEventListener("click", () => {
                    callbackCheckbox.classList.toggle("active");
                    callbackCheckboxInput.checked
                        ? (callbackCheckboxInput.checked = false)
                        : (callbackCheckboxInput.checked = true);
                });

                loadBtn.addEventListener("click", (e) => {
                    e.preventDefault();
                    loadInput.click();
                });

                function updateLoadList() {
                    const files = loadInput.files;
                    const lastItem = loadDelBtn;
                    loadList.innerHTML = "";

                    for (let i = 0; i < files.length; i++) {
                        const file = files[i];
                        const listItem = document.createElement("li");
                        listItem.className = "callback-form__load-item";
                        listItem.textContent = file.name;

                        // Вставляем элемент удаления списка файлов
                        loadList.appendChild(lastItem);

                        // Вставляем новый элемент перед последним
                        loadList.insertBefore(listItem, lastItem);
                    }

                    if (files.length) {
                        loadLabel.classList.add("hide");
                        loadDelBtn.classList.add("show");
                    } else {
                        loadLabel.classList.remove("hide");
                        loadDelBtn.classList.add("show");
                    }
                }

                loadInput.addEventListener("change", () => {
                    updateLoadList();
                });

                loadDelBtn.addEventListener("click", () => {
                    loadInput.value = "";
                    updateLoadList();
                    console.log(loadInput.files);
                });

                contactItem.forEach((item) => {
                    const contactRadio = item.querySelector(
                        ".callback-form__contact-radio"
                    );
                    const contactTitle = item.querySelector(
                        ".callback-form__contact-title"
                    );

                    item.addEventListener("click", () => {
                        if (!contactRadio.checked) {
                            contactItem.forEach((el) => {
                                el.classList.remove("active");
                            });

                            contactRadio.checked = true;
                            item.classList.add("active");
                            contactInput.setAttribute(
                                "placeholder",
                                contactTitle.innerHTML
                            );
                        }

                        console.log(contactInput);
                    });
                });

                submitBtn.addEventListener("click", (e) => {
                    e.preventDefault();
                    const fileList = form.querySelector(
                        ".callback-form__load-input"
                    ).files;

                    console.log("Uploading files:", fileList);
                });
            }
        });
    }

    callbackFormActions();

    if (document.querySelector(".partners-ticker") !== null) {
        const partnersTicker = new Splide(".partners-ticker", {
            fixedHeight: "150px",
            gap: "100px",
            type: "loop",
            drag: false,
            focus: "center",
            perPage: 6,
            arrows: false,
            pagination: false,
            autoScroll: {
                speed: 0.5,
                pauseOnHover: false,
                pauseOnFocus: true,
            },
            breakpoints: {
                1919: { perPage: 4, gap: "65px" },
                1439: {
                    fixedHeight: "100px",
                },
                1023: {
                    fixedHeight: "60px",
                },
                767: { perPage: 3, gap: "16px", fixedHeight: "45px" },
            },
        });

        partnersTicker.mount(window.splide.Extensions);
    }
});
