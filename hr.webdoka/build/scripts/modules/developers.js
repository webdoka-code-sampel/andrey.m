document.addEventListener("DOMContentLoaded", () => {
    const elements = {
        searchInput: document.querySelector(".search-form__search-input"),
        filterItem: document.querySelectorAll(".search-form__filter-item"),
        filterResestBtn: document.querySelector(".search-form__filter-reset"),
        cardSkillsList: document.querySelectorAll(
            ".developers-card__skills-list"
        ),
    };

    function skillsFilter() {
        const { filterItem, searchInput, filterResestBtn } = elements;

        let selectedValues = [];

        filterItem.forEach((item) => {
            if (item !== null) {
                const itemInput = item.querySelector(
                    ".search-form__filter-input"
                );

                item.addEventListener("click", () => {
                    if (!itemInput.checked) {
                        itemInput.checked = true;
                        item.classList.add("active");
                        selectedValues.push(itemInput.value);
                    } else {
                        itemInput.checked = false;
                        item.classList.remove("active");
                        const index = selectedValues.indexOf(itemInput.value);

                        if (index !== -1) {
                            selectedValues.splice(index, 1);
                        }
                    }

                    searchInput.value = selectedValues.join(", ");

                    selectedValues.length > 0
                        ? filterResestBtn.classList.add("show")
                        : filterResestBtn.classList.remove("show");

                    console.log(selectedValues);
                });

                filterResestBtn.addEventListener("click", () => {
                    itemInput.checked = false;

                    searchInput.value = "";
                    filterResestBtn.classList.remove("show");
                    item.classList.remove("active");
                    selectedValues = [];
                    console.log(selectedValues);
                });
            }
        });
    }

    skillsFilter();

    function skillsHide() {
        const { cardSkillsList } = elements;

        cardSkillsList.forEach((block) => {
            if (block !== null) {
                const skillsItem = block.querySelectorAll(
                    ".developers-card__skills-item"
                );

                let skillsRows;
                let gapRows;

                if (window.innerWidth >= 1440) {
                    skillsRows = 2;
                    gapRows = 1;
                } else if (window.innerWidth >= 1024) {
                    skillsRows = 3;
                    gapRows = 2;
                } else {
                    skillsRows = 2;
                    gapRows = 1;
                }

                // Получаем видимую высоту контейнера (равную двум строкам)
                let visibleArea =
                    parseFloat(window.getComputedStyle(block).gap) * gapRows +
                    skillsItem[0].offsetHeight * skillsRows;

                // Счетчик элементов за пределами второй строки
                let elementsBeyondSecondRow = 0;

                for (let i = 0; i < skillsItem.length; i++) {
                    const item = skillsItem[i];
                    const itemDistanceToTop = item.offsetTop - block.offsetTop;
                    if (itemDistanceToTop >= visibleArea) {
                        item.remove();
                        elementsBeyondSecondRow++;
                    }
                }

                if (elementsBeyondSecondRow > 0) {
                    const elemAmount = document.createElement("li");
                    elemAmount.className = "developers-card__skills-item";
                    elemAmount.innerHTML = "+" + elementsBeyondSecondRow;

                    block.appendChild(elemAmount);

                    const itemDistanceToTop =
                        elemAmount.offsetTop - block.offsetTop;
                    console.log(visibleArea);
                    console.log(itemDistanceToTop);

                    if (itemDistanceToTop > visibleArea) {
                        const items = block.querySelectorAll(
                            ".developers-card__skills-item"
                        );
                        elementsBeyondSecondRow++;
                        elemAmount.innerHTML = "+" + elementsBeyondSecondRow;
                        items[items.length - 2].remove();
                    }
                }
            }
        });
    }

    skillsHide();
});
