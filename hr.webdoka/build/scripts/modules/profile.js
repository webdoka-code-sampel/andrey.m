document.addEventListener("DOMContentLoaded", () => {
    const elements = {
        projectsCard: document.querySelectorAll(".projects-card"),
    };

    function stackItemHide() {
        const { projectsCard } = elements;

        projectsCard.forEach((card) => {
            if (card !== null) {
                const stackList = card.querySelectorAll(
                    ".projects-card__stack-list"
                );

                stackList.forEach((list) => {
                    if (list !== null) {
                        const stackItem = list.querySelectorAll(
                            ".projects-card__stack-item"
                        );

                        // Получаем видимую высоту контейнера (равную двум строкам)
                        let visibleArea =
                            parseFloat(window.getComputedStyle(list).gap) +
                            stackItem[0].offsetHeight * 2;

                        // Счетчик элементов за пределами второй строки
                        let elementsBeyondSecondRow = 0;

                        for (let i = 0; i < stackItem.length; i++) {
                            const item = stackItem[i];
                            const itemDistanceToTop =
                                item.offsetTop - list.offsetTop;
                            if (itemDistanceToTop >= visibleArea) {
                                item.remove();
                                elementsBeyondSecondRow++;
                            }
                        }

                        if (elementsBeyondSecondRow > 0) {
                            const elemAmount = document.createElement("li");
                            elemAmount.className = "projects-card__stack-item";
                            elemAmount.innerHTML =
                                "+" + elementsBeyondSecondRow;

                            list.appendChild(elemAmount);

                            const itemDistanceToTop =
                                elemAmount.offsetTop - list.offsetTop;
                            console.log(visibleArea);
                            console.log(itemDistanceToTop);

                            if (itemDistanceToTop > visibleArea) {
                                const items = list.querySelectorAll(
                                    ".projects-card__stack-item"
                                );
                                elementsBeyondSecondRow++;
                                elemAmount.innerHTML =
                                    "+" + elementsBeyondSecondRow;
                                items[items.length - 2].remove();
                            }
                        }
                    }
                });
            }
        });
    }

    stackItemHide();
});
