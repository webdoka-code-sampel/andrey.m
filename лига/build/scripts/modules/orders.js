"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const productCardsHidding = () => {
        const ordersListWrap = document.querySelector(".orders-list");
        console.log(window.innerWidth);
        const windowWidth = {
            mobile: 768,
            tablet: 1024,
            Desktop: 1366,
        };

        const productShow = (width) => {
            if (width < 1366 && width >= 768) {
                // console.log(width);
                return 3;
            } else if (width < 768) {
                // console.log(width);
                return 5;
            } else {
                return 4;
            }
        };

        const getCorrectWordEnding = (number, words) => {
            const lastDigit = number % 10;
            const lastTwoDigits = number % 100;

            if (lastTwoDigits >= 11 && lastTwoDigits <= 19) {
                return words[2]; // товаров
            }

            if (lastDigit === 1) {
                return words[0]; // товар
            }

            if (lastDigit >= 2 && lastDigit <= 4) {
                return words[1]; // товара
            }

            return words[2]; // товаров
        };

        const words = ["товар", "товара", "товаров"];

        if (ordersListWrap !== null) {
            const orderslist =
                ordersListWrap.querySelectorAll(".orders-list__item");

            orderslist.forEach((order) => {
                if (order !== null) {
                    const productList = order.querySelector(
                        ".orders-list__product-list"
                    );
                    const productItem = order.querySelectorAll(
                        ".orders-list__product-item"
                    );

                    if (
                        productItem.length >
                        productShow(window.innerWidth) + 1
                    ) {
                        productItem.forEach((item, i) => {
                            if (item !== null) {
                                if (i > productShow(window.innerWidth) - 1) {
                                    item.remove();
                                }
                            }
                        });

                        const elem = document.createElement("li");
                        elem.classList.add("orders-list__product-link");
                        elem.innerHTML = `Еще ${
                            productItem.length - productShow(window.innerWidth)
                        } ${getCorrectWordEnding(
                            productItem.length - productShow(window.innerWidth),
                            words
                        )}`;
                        productList.appendChild(elem);
                    }
                }
            });
        }
    };

    productCardsHidding();
});
