"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const setProductData = () => {
        const getProductData = async () => {
            try {
                const response = await fetch(
                    `../assets/api/productDetail/index.json`
                );

                if (response.ok) {
                    let data = await response.json();
                    console.log(data);
                    colorSelect(data.product);
                } else {
                    console.error(`Ошибка HTTP: ${response.status}`);
                    showAllResultsBtn.classList.remove("show");
                }
            } catch (error) {
                console.error("Ошибка при запросе:", error);
            }
        };

        getProductData();

        const colorSelectBlock = document.querySelector(".color-select");
        const colorSelectList = colorSelectBlock.querySelector(
            ".color-select__list"
        );

        const currentColorElem = colorSelectBlock.querySelector(
            ".color-select__current-color"
        );

        let currentColor = null;

        const colorSelect = (product) => {
            currentColor = product.currentColor;
            console.log(currentColor);
            currentColorElem.innerHTML = currentColor;

            const setElements = (i) => {
                let el = product.data[i];
                let item = document.createElement("div");
                item.classList.add("color-select__item");

                item.innerHTML = `
                                <input
                                    type="radio"
                                    class="color-select__item-radio"
                                    id="${el.id}"
                                    name="color"
                                    value="${el.value}"
                                />
                    
                                <img
                                    class="color-select__item-img"
                                    src="${el.colorImg}"
                                    alt="couch"
                                />
                            `;

                colorSelectList.appendChild(item);
            };

            if (product.data.length <= 8) {
                for (let i = 0; i < 8; i++) {
                    setElements(i);
                }
            } else {
                for (let i = 0; i < 7; i++) {
                    setElements(i);
                }

                let item = document.createElement("div");
                item.classList.add("color-select__btn-more");

                item.innerHTML = `+${product.data.length - 7}`;

                colorSelectList.appendChild(item);
            }

            const colorSelectItem = colorSelectList.querySelectorAll(
                ".color-select__item"
            );

            colorSelectItem.forEach((item) => {
                if (item !== null) {
                    const inputRadio = item.querySelector(
                        ".color-select__item-radio"
                    );

                    if (Number(inputRadio.id) === product.id) {
                        inputRadio.checked = true;
                        item.classList.add("active");
                    }

                    item.addEventListener("click", () => {
                        colorSelectItem.forEach((el) => {
                            el.classList.remove("active");
                        });

                        item.classList.add("active");
                        inputRadio.checked = true;
                        console.log(inputRadio.value);
                        currentColor = inputRadio.value;
                        currentColorElem.innerHTML = currentColor;
                    });
                }
            });
        };

        const sizeSelect = () => {
            const sizeSelectList = document.querySelector(".size-select__list");
            if (sizeSelectList !== null) {
                const sizeSelectItem =
                    sizeSelectList.querySelectorAll(".size-select__item");

                sizeSelectItem.forEach((select) => {
                    const selectRadio = select.querySelector(
                        ".size-select__item-radio"
                    );

                    select.addEventListener("click", () => {
                        sizeSelectItem.forEach((el) => {
                            el.classList.remove("active");
                        });

                        select.classList.add("active");
                        selectRadio.checked = true;
                    });
                });
            }
        };

        sizeSelect();

        const configurationSelect = () => {
            const configurationSelect = document.querySelectorAll(
                ".product-configuration__select"
            );

            const removeActiveClass = (el) => {
                el.classList.remove("active");
                setTimeout(() => {
                    el.querySelector(
                        ".product-configuration__options-wrap"
                    ).style.zIndex = null;
                }, 200);
            };

            configurationSelect.forEach((select) => {
                const curentConfiguration = select.querySelector(
                    ".product-configuration__current"
                );
                const currentOption = select.querySelector(
                    ".product-configuration__current-desc"
                );

                curentConfiguration.addEventListener("click", () => {
                    if (!select.classList.contains("active")) {
                        configurationSelect.forEach((el) => {
                            if (select !== el) {
                                removeActiveClass(el);
                            }
                        });

                        select.classList.add("active");
                        select.querySelector(
                            ".product-configuration__options-wrap"
                        ).style.zIndex = 1;
                    } else {
                        removeActiveClass(select);
                    }
                });

                const configurationOption = select.querySelectorAll(
                    ".product-configuration__option"
                );

                configurationOption.forEach((option) => {
                    const optionRadio = option.querySelector(
                        ".product-configuration__option-radio"
                    );

                    option.addEventListener("click", () => {
                        if (!option.classList.contains("active")) {
                            configurationOption.forEach((el) => {
                                if (option !== el) {
                                    el.classList.remove("active");
                                }
                            });

                            option.classList.add("active");
                            optionRadio.checked = true;
                            currentOption.innerHTML = optionRadio.value;
                            removeActiveClass(select);
                        } else {
                            removeActiveClass(select);
                        }
                    });
                });
            });

            window.addEventListener("click", (e) => {
                if (!e.target.closest(".product-configuration__select")) {
                    configurationSelect.forEach((el) => {
                        removeActiveClass(el);
                    });
                }
            });
        };

        configurationSelect();

        const tabSelect = () => {
            const tabsBlock = document.querySelector(".preview-tabs");

            if (tabsBlock !== null) {
                const tabsItems = tabsBlock.querySelectorAll(
                    ".preview-tabs__item"
                );
                const tabsDescItems = tabsBlock.querySelectorAll(
                    ".preview-tabs__desc-item"
                );

                let currentTabIndex = 0;

                tabsItems[currentTabIndex].classList.add("active");
                tabsDescItems[currentTabIndex].classList.add("active");

                tabsItems.forEach((tab, i) => {
                    tab.addEventListener("click", () => {
                        if (i !== currentTabIndex) {
                            tabsItems[currentTabIndex].classList.remove(
                                "active"
                            );
                            tabsDescItems[currentTabIndex].classList.remove(
                                "active"
                            );

                            currentTabIndex = i;

                            tabsItems[currentTabIndex].classList.add("active");
                            tabsDescItems[currentTabIndex].classList.add(
                                "active"
                            );
                        }
                    });
                });
            }
        };

        tabSelect();

        const productbar = () => {
            const productbarBlock = document.querySelector(".productbar");
            const mainInfoBlock = document.querySelector(".main-info");

            const productbarBlockHeight = productbarBlock.offsetHeight;
            const mainInfoBlockRect = mainInfoBlock.getBoundingClientRect();
            const mainInfoBlockEdge =
                mainInfoBlockRect.top +
                mainInfoBlockRect.height -
                productbarBlockHeight;

            window.addEventListener("scroll", () => {
                // Проверяем, если прокрутка достигла 46px
                if (window.scrollY >= mainInfoBlockEdge) {
                    productbarBlock.classList.add("show");
                } else {
                    productbarBlock.classList.remove("show");
                }
            });
        };

        productbar();

        const swiper = new Swiper(".swiper", {
            slidesPerView: 3,
            longSwipes: false,
            speed: 1000,
            spaceBetween: 20,
            grabCursor: true,

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    };

    setProductData();
});
