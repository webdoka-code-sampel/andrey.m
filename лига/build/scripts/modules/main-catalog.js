"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const cardImgSwiper = () => {
        const catalogCardList = document.querySelectorAll(".catalog-card");

        if (catalogCardList.length) {
            catalogCardList.forEach((card) => {
                if (card !== null) {
                    const cardImgListWrap = card.querySelector(
                        ".catalog-card__img-wrap"
                    );
                    const cardImgList =
                        card.querySelectorAll(".catalog-card__img");
                    const slidebar = card.querySelector(
                        ".catalog-card__slidebar"
                    );

                    const slideEdgeWidth =
                        cardImgListWrap.offsetWidth / cardImgList.length;
                    let xPos;
                    let slidePosOver;
                    let currentSlide = 0;

                    cardImgList[currentSlide].classList.add("active");

                    if (window.innerWidth >= 1024) {
                        for (let i = 0; i < cardImgList.length; i++) {
                            const slidebarItem = document.createElement("span");
                            slidebarItem.classList.add(
                                "catalog-card__slidebar-item"
                            );
                            slidebar.appendChild(slidebarItem);
                        }

                        const slidebarItem = card.querySelectorAll(
                            ".catalog-card__slidebar-item"
                        );

                        slidebarItem[currentSlide].classList.add("active");

                        cardImgListWrap.addEventListener("mousemove", (e) => {
                            e.stopPropagation();

                            xPos =
                                e.clientX -
                                cardImgListWrap.getBoundingClientRect().left;
                            slidePosOver = Math.ceil(xPos / slideEdgeWidth) - 1;

                            if (slidePosOver >= 0) {
                                cardImgList[currentSlide].classList.remove(
                                    "active"
                                );
                                slidebarItem[currentSlide].classList.remove(
                                    "active"
                                );
                                currentSlide = slidePosOver;
                                cardImgList[currentSlide].classList.add(
                                    "active"
                                );
                                slidebarItem[currentSlide].classList.add(
                                    "active"
                                );
                                console.log("Текуший слайд:" + currentSlide);

                                if (cardImgList[currentSlide].dataset.src) {
                                    cardImgList[currentSlide].setAttribute(
                                        "src",
                                        cardImgList[currentSlide].dataset.src
                                    );

                                    cardImgList[currentSlide].removeAttribute(
                                        "data-src"
                                    );
                                }
                            }
                        });

                        cardImgListWrap.addEventListener("mouseleave", () => {
                            cardImgList[currentSlide].classList.remove(
                                "active"
                            );
                            slidebarItem[currentSlide].classList.remove(
                                "active"
                            );
                            currentSlide = 0;
                            cardImgList[currentSlide].classList.add("active");
                            slidebarItem[currentSlide].classList.add("active");
                            console.log("Текуший слайд:" + currentSlide);
                        });
                    }
                }
            });
        }
    };

    cardImgSwiper();
});
