"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const sortCatalog = () => {
        const sortCatalogBlock = document.querySelector(
            ".current-filters__select"
        );

        if (sortCatalogBlock !== null) {
            const currentOptionBtn = sortCatalogBlock.querySelector(
                ".current-filters__select-current"
            );
            const sortOptionsBlock = sortCatalogBlock.querySelector(
                ".current-filters__options"
            );
            const currentOptionElem = sortCatalogBlock.querySelector(
                ".current-filters__current-text"
            );
            const sortOptions = sortCatalogBlock.querySelectorAll(
                ".current-filters__options-item"
            );

            let currentOption;

            currentOptionBtn.addEventListener("click", () => {
                sortCatalogBlock.classList.toggle("active");
            });

            sortOptionsBlock.addEventListener("mouseleave", () => {
                sortCatalogBlock.classList.remove("active");
            });

            sortOptions.forEach((option, i) => {
                const optionsTitle = option.querySelector(
                    ".current-filters__options-title"
                );
                const optionsRadio = option.querySelector(
                    ".current-filters__options-radio"
                );

                if (optionsRadio.checked) {
                    option.classList.add("active");
                    currentOption = i;
                    currentOptionElem.innerHTML = optionsTitle.innerHTML;
                }

                if (option.classList.contains("active")) {
                    currentOption = i;
                    currentOptionElem.innerHTML = optionsTitle.innerHTML;
                    optionsRadio.checked = true;
                }

                option.addEventListener("click", () => {
                    if (i !== currentOption) {
                        sortOptions[currentOption].classList.remove("active");
                        currentOption = i;
                        option.classList.add("active");
                        currentOptionElem.innerHTML = optionsTitle.innerHTML;
                        optionsRadio.checked = true;
                    }

                    sortCatalogBlock.classList.remove("active");
                });
            });

            document.addEventListener("click", (e) => {
                if (!e.target.closest(".current-filters__select")) {
                    sortCatalogBlock.classList.remove("active");
                }
            });
        }
    };

    sortCatalog();

    const catalogFilters = () => {
        const filterForm = document.querySelector(
            ".catalog-section__filters-wrap"
        );
        const filtersSection = document.querySelector(
            ".catalog-section__filters"
        );
        const filterBlock = filtersSection.querySelectorAll(".filter");
        const filterItems = filtersSection.querySelectorAll(".filter__item");
        const filterCategories = document.querySelectorAll(
            ".filter-categories__category"
        );
        const filterItemsColor = document.querySelectorAll(
            ".filter__item--color"
        );
        const allFiltersResetBtn = document.querySelector(
            ".catalog-section__reset-btn"
        );
        const currentFiltersResetBtn = document.querySelector(
            ".current-filters__list-reset"
        );
        const currentFilters = document.querySelector(".current-filters__list");
        const filterBtn = document.querySelector(".filter-btn");
        const filterCloseBtn = document.querySelector(
            ".catalog-section__filters-close"
        );
        const resultBtn = document.querySelector(
            ".catalog-section__result-btn"
        );

        let selectTimer; // Таймер для отслеживания времени между выбором фильтров
        const doneSelectInterval = 1000; // Таймаут в миллисекундах (1 секунда)

        const sendRequest = async (params) => {
            try {
                //запрос поиска
                console.log("Отправка запроса:", params);

                const response = await fetch(`https://...`);

                if (response.ok) {
                    let data = await response.json();
                } else {
                    console.error(`Ошибка HTTP: ${response.status}`);
                }
            } catch (error) {
                console.error("Ошибка при запросе:", error);
            }
        };

        // Функция, вызываемая при выборе фильтра
        const selectFilter = () => {
            clearTimeout(selectTimer); // Сброс таймера при каждом нажатии клавиши

            // Если прошло достаточно времени с момента последнего нажатия клавиши, отправляем запрос
            selectTimer = setTimeout(() => {
                const formData = new FormData(filterForm);
                const entries = Array.from(formData.entries());
                sendRequest(entries);
            }, doneSelectInterval);
        };

        // Наблюдатель списка текущих фильтров
        const currentFiltersItemsObserver = () => {
            const currentFiltersItems = document.querySelectorAll(
                ".current-filters__item"
            );

            if (!currentFiltersItems.length) {
                currentFiltersResetBtn.classList.remove("show");
            } else {
                currentFiltersResetBtn.classList.add("show");
            }
        };

        // Создаем элемент в блоке текущего фильтра
        const createCurrentFilterElem = (itemInput) => {
            const inputDataValue = itemInput.getAttribute("data-value");
            const inputDataName = itemInput.getAttribute("data-name");

            const currentFiltersItem = document.createElement("li");
            currentFiltersItem.classList.add("current-filters__item");
            currentFiltersItem.setAttribute("data-name", inputDataName);
            currentFiltersItem.setAttribute("data-value", inputDataValue);

            if (itemInput.hasAttribute("data-type")) {
                currentFiltersItem.setAttribute(
                    "data-type",
                    itemInput.getAttribute("data-type")
                );
            }

            let colorElem = "";

            if (itemInput.hasAttribute("data-color")) {
                colorElem = `
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="21"
                                height="20"
                                viewBox="0 0 21 20"
                                fill="${itemInput.getAttribute("data-color")}"
                            >
                                <circle
                                    cx="10.25"
                                    cy="10"
                                    r="9.5"
                                    fill="${itemInput.getAttribute(
                                        "data-color"
                                    )}"
                                />
                            </svg>`;
            }

            currentFiltersItem.innerHTML = `
                        <span class="current-filters__item-decs">
                            <span class="current-filters__item-title">${inputDataName}:</span>
                            ${colorElem}
                            <span class="current-filters__item-value">${inputDataValue}</span>
                        </span>

                        <button class="current-filters__item-del">
                            <img
                                class="current-filters__item-icon"
                                src="../assets/images/icons/close-icon.svg"
                                alt="close-icon"
                            />
                        </button>`;

            currentFilters.appendChild(currentFiltersItem);
            currentFiltersItemsObserver();
            currentFiltersList();
        };

        const currentFiltersList = () => {
            const currentFiltersItems = document.querySelectorAll(
                ".current-filters__item"
            );

            currentFiltersItems.forEach((item) => {
                const itemsWrap = item.parentElement;
                const dataValue = item.getAttribute("data-value");
                const dataName = item.getAttribute("data-name");

                item.addEventListener("click", () => {
                    item.remove();
                    selectFilter();
                    const items = itemsWrap.querySelectorAll(
                        ".current-filters__item"
                    );

                    if (!items.length) {
                        currentFiltersResetBtn.classList.remove("show");
                    }

                    filterItems.forEach((el) => {
                        const filterInput = el.querySelector(
                            ".filter__item-input"
                        );
                        const filterRange = el.querySelectorAll(
                            ".filter__item-input"
                        );

                        const inputDataValue =
                            filterInput.getAttribute("data-value");
                        const inputDataName =
                            filterInput.getAttribute("data-name");

                        if (
                            dataValue === inputDataValue &&
                            dataName === inputDataName
                        ) {
                            filterInput.checked = false;
                            el.classList.remove("active");
                        }

                        filterRange.forEach((range) => {
                            if (
                                range.type === "number" &&
                                dataName === inputDataName
                            ) {
                                range.value = "";
                            }
                        });
                    });
                });
            });
        };

        // Окрыть/Закрыть фильтр
        filterBlock.forEach((filter) => {
            const filterOpenBtn = filter.querySelector(".filter__title");
            const filterList = filter.querySelector(".filter__list");
            const items = filter.querySelectorAll(".filter__item");
            const showMoreBtn = filter.querySelector(".filter__show-btn");

            const showBtnStatus = {
                isOpen: false,
                close: "Показать еще",
                open: "Свернуть",
            };

            // Скрыть фильтры если длина списка бошльше 6
            if (items.length <= 6) {
                showMoreBtn.classList.toggle("hidden");
            }

            // Смена видимости фильтров
            const switchItemsVisibility = () => {
                items.forEach((item, idx) => {
                    if (idx > 5) {
                        item.classList.toggle("hidden");
                    }
                });
            };

            const preOpenFilterBlock = () => {
                if (filter.classList.contains("open")) {
                    filterOpenBtn.classList.add("active");
                    filterList.classList.add("show");

                    if (showBtnStatus.isOpen) {
                        showBtnStatus.isOpen = false;
                        showMoreBtn.innerHTML = showBtnStatus.close;
                    }
                    filterList.style.maxHeight = filterList.scrollHeight + "px";
                    switchItemsVisibility();
                }

                items.forEach((item) => {
                    const inputRadio = item.querySelector(
                        'input[type="radio"]'
                    );
                    const inputCheckbox = item.querySelector(
                        'input[type="checkbox"]'
                    );
                    const inputRange = item.querySelectorAll(
                        'input[type="number"]'
                    );

                    const openFilter = () => {
                        item.classList.add("active");
                        filterList.classList.add("show");
                        filterOpenBtn.classList.add("active");
                        filterList.style.maxHeight =
                            filterList.scrollHeight + "px";
                        switchItemsVisibility();
                    };

                    if (inputRadio !== null) {
                        if (inputRadio.checked) {
                            openFilter();
                            createCurrentFilterElem(inputRadio);
                        }
                    }

                    if (inputCheckbox !== null) {
                        if (inputCheckbox.checked) {
                            openFilter();
                            createCurrentFilterElem(inputCheckbox);
                        }
                    }

                    if (inputRange[0] !== undefined) {
                        if (inputRange[0].value && inputRange[1].value) {
                            openFilter();
                        }
                    }
                });
            };

            if (window.innerWidth >= 1024) {
                preOpenFilterBlock();
            }

            // Открыть полный список фильров
            filterOpenBtn.addEventListener("click", () => {
                filterOpenBtn.classList.toggle("active");
                filterList.classList.toggle("show");

                if (filter.classList.contains("open")) {
                    filter.classList.remove("open");
                    filterList.style.maxHeight = null;
                    if (!showBtnStatus.isOpen) {
                        setTimeout(() => {
                            switchItemsVisibility();
                        }, 100);
                    }
                } else {
                    if (filterList.style.maxHeight) {
                        filterList.style.maxHeight = null;
                        if (!showBtnStatus.isOpen) {
                            setTimeout(() => {
                                switchItemsVisibility();
                            }, 100);
                        }
                    } else {
                        if (showBtnStatus.isOpen) {
                            showBtnStatus.isOpen = false;
                            showMoreBtn.innerHTML = showBtnStatus.close;
                        }
                        filterList.style.maxHeight =
                            filterList.scrollHeight + "px";
                        switchItemsVisibility();
                    }
                }
            });

            showMoreBtn.addEventListener("click", (e) => {
                e.preventDefault();
                if (!showBtnStatus.isOpen) {
                    showMoreBtn.innerHTML = showBtnStatus.open;
                    showBtnStatus.isOpen = true;
                } else {
                    showMoreBtn.innerHTML = showBtnStatus.close;
                    showBtnStatus.isOpen = false;
                }
                switchItemsVisibility();
            });

            // Кнопка открытия мобильного фильтра
            filterBtn.addEventListener("click", () => {
                filtersSection.classList.add("show");
                document.body.style.overflow = "hidden";
                preOpenFilterBlock();
            });
        });

        // Открыть/закрыть Категории
        filterCategories.forEach((category) => {
            const categoryOpenBtn = category.querySelector(
                ".filter-categories__category-title"
            );
            const categoryList = category.querySelector(
                ".filter-categories__list"
            );

            if (categoryList !== null) {
                categoryOpenBtn.addEventListener("click", () => {
                    categoryOpenBtn.classList.toggle("active");

                    categoryList.style.maxHeight
                        ? (categoryList.style.maxHeight = null)
                        : (categoryList.style.maxHeight =
                              categoryList.scrollHeight + "px");
                });
            }
        });

        // Установка цвета в фильтр "Цвет"
        filterItemsColor.forEach((item) => {
            const input = item.querySelector(".filter__item-input");
            const dataColor = input.getAttribute("data-color");
            const colorElem = item.querySelector("svg");
            const colorElemCircle = colorElem.querySelector("circle");
            colorElemCircle.setAttribute("fill", dataColor);
        });

        // Управление фильтрами
        filterItems.forEach((item) => {
            const filterInput = item.querySelector(".filter__item-input");
            const filterRangeInput = item.querySelectorAll(
                ".filter__range-input"
            );
            // Категории фильтров
            const filterWrap = filterInput.closest(".filter__list");
            const items = filterWrap.querySelectorAll(".filter__item");

            let previousElem = null;

            // Управление checkbox и radio
            item.addEventListener("click", () => {
                item.classList.toggle("active");
                const currentFiltersItems = document.querySelectorAll(
                    ".current-filters__item"
                );

                // Определение фильтра по категории
                items.forEach((i) => {
                    const itemInput = i.querySelector(".filter__item-input");
                    const rangeInputs = i.querySelectorAll(
                        ".filter__range-input"
                    );
                    const inputDataValue = itemInput.getAttribute("data-value");
                    const inputDataName = itemInput.getAttribute("data-name");

                    // Добавляем value в range при клике на фильтр с заданными параметрами
                    if (filterInput.type !== "number") {
                        rangeInputs.forEach((range, idx) => {
                            if (filterInput.dataset.min) {
                                rangeInputs[0].value = filterInput.dataset.min;
                                rangeInputs[1].value = filterInput.dataset.max;
                            }
                        });
                    }

                    // Управление состоянием текущих фильтров
                    currentFiltersItems.forEach((el) => {
                        const dataValue = el.getAttribute("data-value");
                        const dataName = el.getAttribute("data-name");

                        if (filterInput.type === "radio") {
                            if (filterInput.checked) {
                                rangeInputs.forEach((range) => {
                                    console.log(range);
                                    // Сброс value у range при клике на фильтр с заданными параметрами
                                    range.value = "";
                                });
                            }

                            if (itemInput.checked) {
                                i.classList.remove("active");
                                deleteCurrentFilterElem(itemInput);
                            }

                            if (
                                dataValue !== inputDataValue &&
                                dataName === inputDataName
                            ) {
                                el.remove();
                            }
                        }

                        if (filterInput.type === "checkbox") {
                            rangeInputs.forEach((range) => {
                                if (dataName === range.dataset.name) {
                                    // Сброс value у range при клике на фильтр с заданными параметрами
                                    range.value = "";
                                    el.remove();
                                }
                            });
                        }
                    });
                });

                // Упаравление состоянием checkbox и radio
                if (filterInput.checked && filterInput.type !== "number") {
                    filterInput.checked = false;
                    deleteCurrentFilterElem(filterInput);
                    // sendRequest(filterInput);
                    selectFilter();
                } else {
                    filterInput.checked = true;

                    if (filterInput.type !== "number") {
                        if (window.innerWidth < 1024) {
                            resultBtn.parentElement.classList.add("show");
                        }
                        createCurrentFilterElem(filterInput);
                        // sendRequest(filterInput);
                        selectFilter();
                    }
                }
            });

            // Добавляем в текущие фильтры при загрузке страницы, если уже есть выбранные фильтры
            if (filterRangeInput[0] !== undefined) {
                if (filterRangeInput[0].value && filterRangeInput[1].value) {
                    const inputDataName =
                        filterRangeInput[0].getAttribute("data-name");
                    // Создаем один инпут из значений двух range
                    const elem = document.createElement("input");
                    elem.setAttribute("data-name", inputDataName);
                    elem.setAttribute(
                        "data-value",
                        `${filterRangeInput[0].value} - ${filterRangeInput[1].value}`
                    );
                    elem.setAttribute("data-type", filterRangeInput[0].type);

                    // Добавляем в текущие фильтры новое значение
                    createCurrentFilterElem(elem);
                    previousElem = elem;
                }
            }

            // Управление range(numbers) инпутами
            filterRangeInput.forEach((range) => {
                const rangeWrap = range.closest(".filter__range-wrap");
                const rangeInputs = rangeWrap.querySelectorAll(
                    ".filter__range-input"
                );

                // Отслеживание изменения value
                range.addEventListener("input", () => {
                    const minRange = Number(rangeInputs[0].min);
                    const maxRange = Number(rangeInputs[1].max);
                    const minValue = Number(rangeInputs[0].value);
                    const maxValue = Number(rangeInputs[1].value);

                    let currentMinValue;
                    let currentMaxValue;

                    const createNewInput = () => {
                        const inputDataName =
                            rangeInputs[0].getAttribute("data-name");
                        // Создаем один инпут из значений двух range
                        const elem = document.createElement("input");
                        elem.setAttribute("data-name", inputDataName);
                        elem.setAttribute(
                            "data-value",
                            `${currentMinValue} - ${currentMaxValue}`
                        );
                        elem.setAttribute("data-type", rangeInputs[0].type);

                        // Удаляем предыдущее значение фильтра
                        if (previousElem !== null) {
                            deleteCurrentFilterElem(previousElem);
                        }

                        createCurrentFilterElem(elem);
                        // sendRequest(elem);
                        selectFilter();
                        // Переопределяем новое значение как предыдущее
                        previousElem = elem;
                        if (window.innerWidth < 1024) {
                            resultBtn.parentElement.classList.add("show");
                        }
                    };

                    // Если поле "от" не пустое
                    if (
                        !maxValue &&
                        minValue >= minRange &&
                        minValue < maxRange
                    ) {
                        currentMinValue = minValue;
                        currentMaxValue = maxRange;
                        createNewInput();
                    }

                    // Если поле "до" не пустое
                    if (
                        !minValue &&
                        maxValue >= minRange &&
                        maxValue < maxRange
                    ) {
                        currentMinValue = minRange;
                        currentMaxValue = maxValue;
                        createNewInput();
                    }

                    // Если оба поля value не пусты
                    if (
                        minValue &&
                        maxValue &&
                        minValue < maxValue &&
                        minValue >= minRange &&
                        maxValue < maxRange
                    ) {
                        currentMinValue = minValue;
                        currentMaxValue = maxValue;
                        createNewInput();
                    }

                    // Если оба поля value пустые
                    if (!maxValue && !minValue) {
                        if (previousElem !== null) {
                            deleteCurrentFilterElem(previousElem);
                            // sendRequest(previousElem);
                            selectFilter();
                        }
                    }

                    items.forEach((i) => {
                        const itemInput = i.querySelector(
                            ".filter__item-input"
                        );
                        const currentFiltersItems = document.querySelectorAll(
                            ".current-filters__item"
                        );

                        // Определяем есть ли активные фильтры radio/checkbox в категории
                        if (itemInput.checked) {
                            //Сбрасываем активные radio/checkbox
                            i.classList.remove("active");
                            itemInput.checked = false;

                            // Сравниваем и удаляем из блока текущих фильтров
                            currentFiltersItems.forEach((item) => {
                                const dataValue =
                                    item.getAttribute("data-value");
                                const dataName = item.getAttribute("data-name");
                                const itemInputValue =
                                    itemInput.getAttribute("data-value");
                                const itemInputName =
                                    itemInput.getAttribute("data-name");

                                if (
                                    dataValue === itemInputValue &&
                                    dataName === itemInputName
                                ) {
                                    item.remove();
                                    currentFiltersItemsObserver();
                                }
                            });
                        }
                    });
                });
            });
        });

        // Удаление текущих элементов блока "текущие фильтры", через блок основных фильтров
        const deleteCurrentFilterElem = (itemInput) => {
            const currentFiltersItems = document.querySelectorAll(
                ".current-filters__item"
            );

            if (itemInput !== undefined) {
                currentFiltersItems.forEach((el) => {
                    const dataValue = el.getAttribute("data-value");
                    const dataName = el.getAttribute("data-name");
                    const itemInputName = itemInput.getAttribute("data-name");
                    let itemInputValue;

                    if (itemInput.dataset.type) {
                        itemInputValue = itemInput.getAttribute("data-value");
                    } else {
                        itemInputValue = itemInput.value;
                    }

                    if (
                        dataValue === itemInputValue &&
                        dataName === itemInputName
                    ) {
                        el.remove();
                    }
                });
            } else {
                currentFiltersItems.forEach((el) => {
                    el.remove();
                });
            }

            currentFiltersItemsObserver();
        };

        // Сброс всех фильтров
        const resetFilters = () => {
            filterItems.forEach((item) => {
                const itemInput = item.querySelectorAll(".filter__item-input");

                itemInput.forEach((input) => {
                    if (input.checked) {
                        input.checked = false;
                        item.classList.remove("active");
                    }

                    if (input.type === "number") {
                        input.value = "";
                    }
                });
            });

            deleteCurrentFilterElem();
            // sendRequest();
            selectFilter();
        };

        currentFiltersResetBtn.addEventListener("click", () => {
            resetFilters();
        });

        allFiltersResetBtn.addEventListener("click", (e) => {
            e.preventDefault();
            resetFilters();
        });

        const filterClose = () => {
            document
                .querySelector(".catalog-section__filters-wrap")
                .classList.add("hidding");
            resultBtn.parentElement.classList.remove("show");

            setTimeout(() => {
                document
                    .querySelector(".catalog-section__filters-wrap")
                    .classList.remove("hidding");
                filtersSection.classList.remove("show");
                document.body.style.overflow = null;
            }, 200);
        };

        filterCloseBtn.addEventListener("click", (e) => {
            e.preventDefault();
            filterClose();
        });

        resultBtn.addEventListener("click", (e) => {
            e.preventDefault();
            filterClose();
        });

        document.addEventListener("click", (e) => {
            if (e.target.classList.contains("catalog-section__filters")) {
                document
                    .querySelector(".catalog-section__filters-wrap")
                    .classList.add("hidding");

                setTimeout(() => {
                    document
                        .querySelector(".catalog-section__filters-wrap")
                        .classList.remove("hidding");
                    filtersSection.classList.remove("show");
                    document.body.style.overflow = null;
                }, 200);
            }
        });
    };

    catalogFilters();

    const swiper = new Swiper(".swiper", {
        slidesPerView: 1,
        speed: 1000,
        spaceBetween: 30,
        grabCursor: true,

        // Responsive breakpoints
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 2.9,
                spaceBetween: 18,
            },
            // when window width is >= 576px
            576: {
                slidesPerView: 3.7,
                spaceBetween: 18,
            },
            // when window width is >= 768px
            768: {
                slidesPerView: 4.9,
            },
            // when window width is >= 1024px
            1024: {
                slidesPerView: 7.5,
            },
            // when window width is >= 1920px
            1920: {
                slidesPerView: 8,
            },
        },
    });
});
