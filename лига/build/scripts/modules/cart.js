"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const isCartEmpty = () => {
        const productsCard = document.querySelectorAll(".products-card");

        if (!productsCard.length) {
            const cartSection = document.querySelector(".cart");
            cartSection.classList.add("hidden");
        } else {
            const emptyCartSection = document.querySelector(".empty-cart");
            emptyCartSection.classList.add("hidden");
        }
    };

    isCartEmpty();

    const orderActions = () => {
        const orderForm = document.querySelector(".order-form");

        if (orderForm !== null) {
            const deliveryRadioBtns = orderForm.querySelectorAll(
                ".order-form__delivery-item"
            );
            const deliveryTypeAddresses = orderForm.querySelectorAll(
                ".order-form__address "
            );
            const paymentRadioBtns = orderForm.querySelectorAll(
                ".order-form__payment-item"
            );
            const promocodeBlock = orderForm.querySelector(
                ".order-form__promocode"
            );
            const promocodeBtnOpen = promocodeBlock.querySelector(
                ".order-form__promocode-title"
            );

            const promocodeInputElem = promocodeBlock.querySelector(
                ".order-form__promocode-item"
            );

            deliveryRadioBtns.forEach((btn, i) => {
                if (btn !== null) {
                    const radioInput = btn.querySelector(
                        ".order-form__delivery-input"
                    );

                    btn.addEventListener("click", () => {
                        if (!radioInput.checked) {
                            deliveryRadioBtns.forEach((el) => {
                                el.classList.remove("active");
                                const radio = el.querySelector(
                                    ".order-form__delivery-input"
                                );
                                radio.checked = false;
                            });
                            deliveryTypeAddresses.forEach((el) => {
                                el.classList.remove("show");
                            });

                            btn.classList.add("active");
                            radioInput.checked = true;
                            deliveryTypeAddresses[i].classList.add("show");
                        }
                    });
                }
            });

            paymentRadioBtns.forEach((btn) => {
                if (btn !== null) {
                    const radioInput = btn.querySelector(
                        ".order-form__payment-input"
                    );

                    btn.addEventListener("click", () => {
                        if (!radioInput.checked) {
                            paymentRadioBtns.forEach((el) => {
                                el.classList.remove("active");
                                const radio = el.querySelector(
                                    ".order-form__payment-input"
                                );
                                radio.checked = false;
                            });

                            btn.classList.add("active");
                            radioInput.checked = true;
                        }
                    });
                }
            });

            promocodeBtnOpen.addEventListener("click", () => {
                promocodeBlock.classList.toggle("active");
                if (promocodeInputElem.style.maxHeight) {
                    promocodeInputElem.style.maxHeight = null;
                } else {
                    promocodeInputElem.style.maxHeight =
                        promocodeInputElem.scrollHeight + "px";
                }
            });
        }
    };

    orderActions();

    if (document.querySelector(".bestsellers-swiper") !== null) {
        const bestsellersSwiper = new Swiper(".bestsellers-swiper", {
            slidesPerView: 1,
            speed: 1000,
            spaceBetween: 20,
            grabCursor: true,

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 2.1,
                },
                // when window width is >= 576px
                576: {
                    slidesPerView: 2.5,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 3.2,
                },
                // when window width is >= 1024px
                1024: {
                    slidesPerView: 4.2,
                },
                // when window width is >= 1920px
                1920: {
                    slidesPerView: 6,
                },
            },
        });
    }
});
