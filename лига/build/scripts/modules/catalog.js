"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const hideListItems = () => {
        const catalogBlock = document.querySelector(".catalog-block");

        if (catalogBlock !== null) {
            const catalogList = catalogBlock.querySelectorAll(".catalog-elem");

            catalogList.forEach((elem) => {
                if (elem !== null) {
                    const catalogItemListWrap =
                        elem.querySelector(".catalog-list");
                    const catalogItemList = elem.querySelectorAll(
                        ".catalog-list__item"
                    );

                    const isListOpen = {
                        listOpen: false,
                        openTitle: "Свернуть",
                        closedTitle: "Ещё",
                    };

                    catalogItemList.forEach((item, i) => {
                        if (item !== null) {
                            if (i + 1 > 8) {
                                item.classList.add("hidden");
                            }
                        }
                    });

                    if (catalogItemList.length > 8) {
                        const showMoreBtn = document.createElement("button");
                        showMoreBtn.classList.add("catalog-list__btn-more");
                        showMoreBtn.innerHTML = isListOpen.closedTitle;

                        catalogItemListWrap.appendChild(showMoreBtn);

                        if (showMoreBtn !== null) {
                            showMoreBtn.addEventListener("click", () => {
                                if (!isListOpen.listOpen) {
                                    isListOpen.listOpen = true;

                                    catalogItemList.forEach((item, i) => {
                                        if (item !== null) {
                                            if (i + 1 > 8) {
                                                item.classList.remove("hidden");
                                            }
                                        }
                                    });

                                    showMoreBtn.innerHTML =
                                        isListOpen.openTitle;
                                } else {
                                    isListOpen.listOpen = false;

                                    catalogItemList.forEach((item, i) => {
                                        if (item !== null) {
                                            if (i + 1 > 8) {
                                                item.classList.add("hidden");
                                            }
                                        }
                                    });

                                    showMoreBtn.innerHTML =
                                        isListOpen.closedTitle;
                                }
                            });
                        }
                    }
                }
            });
        }
    };

    hideListItems();
});
