"use strict";

window.addEventListener("DOMContentLoaded", () => {
    const imgBlock = document.querySelectorAll(".idea-block__img-wrap");
    const mediaBreakpoints = {
        desktopSmall: 1366,
        tablet: 1024,
        mobile: 768,
    };

    function defineCurrentScreenType() {
        if (window.innerWidth < mediaBreakpoints.mobile) {
            return 3;
        }

        if (window.innerWidth < mediaBreakpoints.tablet) {
            return 2;
        }

        if (window.innerWidth < mediaBreakpoints.desktopSmall) {
            return 1;
        }

        return 0;
    }

    let currentScreenType = defineCurrentScreenType();

    imgBlock.forEach((block) => {
        if (block !== null) {
            const popupDot = block.querySelector(".popup-dot");
            const popupContent = block.querySelector(".popup-content");

            const blockHeight = block.clientHeight;
            const blockWidth = block.clientWidth;

            popupDot.addEventListener("mouseenter", function () {
                popupContent.classList.add("active");
            });

            block.addEventListener("mouseleave", function () {
                popupContent.classList.remove("active");
            });

            if (popupDot !== null) {
                const popupDataPositionTop = popupDot.dataset.positionTop
                    .split(",")
                    .map(Number);
                const popupDataPositionLeft = popupDot.dataset.positionLeft
                    .split(",")
                    .map(Number);
                let popupDotPositionTop =
                    popupDataPositionTop[currentScreenType];
                let popupDotPositionLeft =
                    popupDataPositionLeft[currentScreenType];

                popupDot.style.top = popupDotPositionTop + "px";
                popupDot.style.left = popupDotPositionLeft + "px";

                if (popupDotPositionTop > blockHeight / 2) {
                    popupContent.style.top =
                        popupDotPositionTop - popupContent.clientHeight + "px";
                } else {
                    popupContent.style.top = popupDotPositionTop + "px";
                }

                if (popupDotPositionLeft > blockWidth / 2) {
                    popupContent.style.left =
                        popupDotPositionLeft -
                        popupContent.clientWidth -
                        5 +
                        "px";
                } else {
                    popupContent.style.left = popupDotPositionLeft + 22 + "px";
                }
            }
        }
    });
});
