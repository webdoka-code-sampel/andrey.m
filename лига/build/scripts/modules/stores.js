"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const storeSelect = () => {
        const storesContentWrapper = document.querySelector(".stores-content");
        if (storesContentWrapper !== null) {
            const storesBlock = document.querySelector(".stores-list");
            const storeDesc = document.querySelector(".stores-desc");
            const mapBlock = document.querySelector(".stores-maps__list");

            let currentStoreIndex = 0;

            const showStoreDesc = () => {
                if (storeDesc !== null) {
                    const storesList =
                        storeDesc.querySelectorAll(".stores-elem");

                    storeDesc.classList.add("show");

                    storesList.forEach((store, i) => {
                        if (store !== null) {
                            if (i !== currentStoreIndex) {
                                store.classList.remove("show");
                            }

                            if (i === currentStoreIndex) {
                                store.classList.add("show");
                            }
                        }
                    });
                }

                if (mapBlock !== null) {
                    const mapItem = mapBlock.querySelectorAll(".maps-item");

                    mapItem.forEach((map, i) => {
                        if (map !== null) {
                            if (i !== currentStoreIndex) {
                                map.classList.remove("show");
                            }

                            if (i === currentStoreIndex) {
                                map.classList.add("show");
                            }
                        }
                    });
                }
            };

            if (storesBlock !== null) {
                const storesList = storesBlock.querySelectorAll(".stores-item");

                storesList.forEach((store, i) => {
                    if (store !== null) {
                        store.addEventListener("click", () => {
                            currentStoreIndex = i;
                            showStoreDesc();
                        });
                    }
                });
            }

            if (storeDesc !== null) {
                const storesList = storeDesc.querySelectorAll(".stores-elem");

                storesList.forEach((store, i) => {
                    if (store !== null) {
                        const descHideBtn = store.querySelector(
                            ".stores-elem__header-top"
                        );

                        if (descHideBtn !== null) {
                            descHideBtn.addEventListener("click", () => {
                                storeDesc.classList.remove("show");
                                store.classList.remove("show");
                            });
                        }
                    }
                });
            }
        }
    };

    storeSelect();

    const swiperMobile = new Swiper(".stores-elem__swiper", {
        slidesPerView: 1,
        speed: 1000,
        spaceBetween: 30,
        grabCursor: true,

        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
        },

        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },

        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
            },
            // when window width is >= 576px
            576: {
                spaceBetween: 45,
                slidesPerView: 1,
            },
            // when window width is >= 768px
            768: {
                slidesPerView: 1,
            },
        },
    });
});
