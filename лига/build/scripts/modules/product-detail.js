"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const mediaBreakpoints = {
        desktopSmall: 1366,
        tablet: 1024,
        mobile: 768,
    };

    const colorSelect = () => {
        const colorSelectBlock = document.querySelector(".color-select");
        const colorSelectItems = colorSelectBlock.querySelectorAll(
            ".color-select__item"
        );
        const currentColorElem = colorSelectBlock.querySelector(
            ".color-select__current-color"
        );
        const colorMoreBtn = colorSelectBlock.querySelector(
            ".color-select__btn-more"
        );

        let currentColor = null;

        if (colorSelectBlock !== null) {
            colorSelectItems.forEach((item, i) => {
                const selectRadio = item.querySelector(
                    ".color-select__item-radio"
                );

                if (window.innerWidth >= 1024) {
                    if (colorSelectItems.length > 8) {
                        colorMoreBtn.innerHTML = `+${
                            colorSelectItems.length - 7
                        }`;
                        colorMoreBtn.classList.add("show");
                        if (i > 6) {
                            item.classList.add("hidden");
                        }
                    }
                } else if (window.innerWidth >= 768) {
                    if (colorSelectItems.length > 5) {
                        colorMoreBtn.innerHTML = `+${
                            colorSelectItems.length - 5
                        }`;
                        colorMoreBtn.classList.add("show");
                        if (i > 4) {
                            item.classList.add("hidden");
                        }
                    }
                }

                colorMoreBtn.addEventListener("click", () => {
                    colorMoreBtn.classList.remove("show");

                    if (window.innerWidth >= 1024) {
                        if (i > 6) {
                            item.classList.remove("hidden");
                        }
                    } else if (window.innerWidth >= 768) {
                        if (i > 5) {
                            item.classList.remove("hidden");
                        }
                    }
                });

                item.addEventListener("click", () => {
                    colorSelectItems.forEach((el) => {
                        el.classList.remove("active");
                    });

                    item.classList.add("active");
                    selectRadio.checked = true;
                    currentColor = selectRadio.value;
                    currentColorElem.innerHTML = currentColor;
                });
            });
        }
    };

    colorSelect();

    const sizeSelect = () => {
        const sizeSelectList = document.querySelector(".size-select__list");
        if (sizeSelectList !== null) {
            const sizeSelectItem =
                sizeSelectList.querySelectorAll(".size-select__item");

            sizeSelectItem.forEach((select) => {
                const selectRadio = select.querySelector(
                    ".size-select__item-radio"
                );

                select.addEventListener("click", () => {
                    sizeSelectItem.forEach((el) => {
                        el.classList.remove("active");
                    });

                    select.classList.add("active");
                    selectRadio.checked = true;
                });
            });
        }
    };

    sizeSelect();

    const configurationSelect = () => {
        const configurationSelect = document.querySelectorAll(
            ".product-configuration__select"
        );

        const removeActiveClass = (el) => {
            el.classList.remove("active");
            setTimeout(() => {
                el.querySelector(
                    ".product-configuration__options-wrap"
                ).style.zIndex = null;
            }, 200);
        };

        configurationSelect.forEach((select) => {
            if (select !== null) {
                const curentConfiguration = select.querySelector(
                    ".product-configuration__current"
                );
                const currentOption = select.querySelector(
                    ".product-configuration__current-desc"
                );

                curentConfiguration.addEventListener("click", () => {
                    if (!select.classList.contains("active")) {
                        configurationSelect.forEach((el) => {
                            if (select !== el) {
                                removeActiveClass(el);
                            }
                        });

                        select.classList.add("active");
                        select.querySelector(
                            ".product-configuration__options-wrap"
                        ).style.zIndex = 1;
                    } else {
                        removeActiveClass(select);
                    }
                });

                const configurationOption = select.querySelectorAll(
                    ".product-configuration__option"
                );

                configurationOption.forEach((option) => {
                    const optionRadio = option.querySelector(
                        ".product-configuration__option-radio"
                    );

                    option.addEventListener("click", () => {
                        if (!option.classList.contains("active")) {
                            configurationOption.forEach((el) => {
                                if (option !== el) {
                                    el.classList.remove("active");
                                }
                            });

                            option.classList.add("active");
                            optionRadio.checked = true;
                            currentOption.innerHTML = optionRadio.value;
                            removeActiveClass(select);
                        } else {
                            removeActiveClass(select);
                        }
                    });
                });
            }
        });

        window.addEventListener("click", (e) => {
            if (!e.target.closest(".product-configuration__select")) {
                configurationSelect.forEach((el) => {
                    removeActiveClass(el);
                });
            }
        });
    };

    configurationSelect();

    const tabSelect = () => {
        const tabsBlock = document.querySelector(".preview-tabs");

        if (tabsBlock !== null) {
            const tabsItems = tabsBlock.querySelectorAll(".preview-tabs__item");
            const tabsDescItems = tabsBlock.querySelectorAll(
                ".preview-tabs__desc-item"
            );
            const showAllCharacteristicsLink = document.querySelector(
                ".product-characteristics__show-all"
            );
            const showFullDescBtn = tabsBlock.querySelector(
                ".preview-tabs__show-full"
            );
            const characteristicsDescText = tabsBlock.querySelector(
                ".preview-tabs__desc-text"
            );
            const infoItems = tabsDescItems[0].querySelectorAll(
                ".preview-tabs__info-item"
            );
            const characteristicsShowAllBtn = tabsBlock.querySelector(
                ".preview-tabs__info-btn"
            );
            const feedbackLink = document.querySelector(".main-info__feedback");

            let currentTabIndex = 0;

            const tabsBlockEdge =
                window.scrollY +
                tabsBlock.getBoundingClientRect().top -
                document.querySelector(".productbar").offsetHeight;

            tabsItems[currentTabIndex].classList.add("active");
            tabsDescItems[currentTabIndex].classList.add("active");

            tabsItems.forEach((tab, i) => {
                tab.addEventListener("click", () => {
                    if (i !== currentTabIndex) {
                        tabsItems[currentTabIndex].classList.remove("active");
                        tabsDescItems[currentTabIndex].classList.remove(
                            "active"
                        );

                        currentTabIndex = i;

                        tabsItems[currentTabIndex].classList.add("active");
                        tabsDescItems[currentTabIndex].classList.add("active");
                    }
                });
            });

            showAllCharacteristicsLink.addEventListener("click", () => {
                if (currentTabIndex !== 0) {
                    tabsItems[currentTabIndex].classList.remove("active");
                    tabsDescItems[currentTabIndex].classList.remove("active");

                    currentTabIndex = 0;
                }
                window.scrollTo(0, tabsBlockEdge - 30);
                tabsItems[currentTabIndex].classList.add("active");
                tabsDescItems[currentTabIndex].classList.add("active");
            });

            feedbackLink.addEventListener("click", () => {
                if (currentTabIndex !== 3) {
                    tabsItems[currentTabIndex].classList.remove("active");
                    tabsDescItems[currentTabIndex].classList.remove("active");

                    currentTabIndex = 3;
                }
                window.scrollTo(0, tabsBlockEdge - 30);
                tabsItems[currentTabIndex].classList.add("active");
                tabsDescItems[currentTabIndex].classList.add("active");
            });

            showFullDescBtn.addEventListener("click", () => {
                characteristicsDescText.classList.toggle("show");
            });

            // infoItems.forEach((item, i) => {
            //     if (i !== 0) {
            //         item.classList.add("hidden");
            //     }

            //     characteristicsShowAllBtn.addEventListener("click", () => {
            //         if (i !== 0) {
            //             item.classList.toggle("hidden");
            //         }
            //     });
            // });
        }
    };

    tabSelect();

    const productbar = () => {
        const productbarBlock = document.querySelector(".productbar");
        const mainInfoBlock = document.querySelector(".main-info");

        if (productbarBlock !== null) {
            const productbarBlockHeight = productbarBlock.offsetHeight;
            const mainInfoBlockRect = mainInfoBlock.getBoundingClientRect();
            const mainInfoBlockEdge =
                window.scrollY +
                mainInfoBlockRect.top +
                mainInfoBlockRect.height -
                productbarBlockHeight;

            window.addEventListener("scroll", () => {
                if (window.scrollY >= mainInfoBlockEdge) {
                    productbarBlock.classList.add("show");
                } else {
                    productbarBlock.classList.remove("show");
                }
            });
        }
    };

    productbar();

    const previewGalleryModal = () => {
        const previewPhotoWrap = document.querySelector(".preview-photo__wrap");

        if (previewPhotoWrap !== null) {
            const previewGalleryBlock =
                previewPhotoWrap.querySelector(".preview-gallery");
            const previewGalleryBlockShowBtn = previewPhotoWrap.querySelector(
                ".preview-photo__show-btn"
            );
            const previewPhoto = previewPhotoWrap.querySelectorAll(
                ".preview-photo__img-wrap"
            );
            const previewGalleryBtnClose = previewPhotoWrap.querySelector(
                ".preview-gallery__btn-close"
            );

            const scrollbarWidth =
                window.innerWidth - document.documentElement.clientWidth;

            const galleryModalOpen = () => {
                previewGalleryBlock.classList.add("show");
                document.body.style.overflow = "hidden";
                document.body.style.marginRight = scrollbarWidth + "px";
            };

            const galleryModalClose = () => {
                previewGalleryBlock.classList.remove("show");
                document.body.style.overflow = null;
                document.body.style.marginRight = null;
            };

            previewGalleryBlockShowBtn.addEventListener("click", () => {
                galleryModalOpen();
            });

            previewPhoto.forEach((photo) => {
                photo.addEventListener("click", () => {
                    galleryModalOpen();
                });
            });

            previewGalleryBtnClose.addEventListener("click", () => {
                galleryModalClose();
            });

            document.addEventListener("click", (e) => {
                if (e.target.classList.contains("preview-gallery")) {
                    galleryModalClose();
                }
            });
        }
    };

    previewGalleryModal();

    const installmentModal = () => {
        const installmentItemList =
            document.querySelectorAll(".installment-item");
        const installmentModal = document.querySelector(".installment-modal");
        const installmentModalBlockList = document.querySelectorAll(
            ".installment-modal__block"
        );
        const scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;

        installmentItemList.forEach((item, idx) => {
            if (item !== null) {
                item.addEventListener("click", () => {
                    installmentModal.classList.add("active");
                    installmentModalBlockList[idx].classList.add("active");
                    document.body.style.overflow = "hidden";
                    document.body.style.marginRight = scrollbarWidth + "px";
                });
            }
        });

        installmentModalBlockList.forEach((block) => {
            if (block !== null) {
                const blockBtnClose = block.querySelector(
                    ".installment-modal__block-close"
                );

                block.addEventListener("click", (e) => {
                    e.stopPropagation();
                });

                blockBtnClose.addEventListener("click", () => {
                    block.classList.remove("active");
                    installmentModal.classList.remove("active");
                    document.body.style.overflow = null;
                    document.body.style.marginRight = null;
                });
            }
        });

        if (installmentModal !== null) {
            installmentModal.addEventListener("click", (e) => {
                if (e.target === installmentModal) {
                    installmentModal.classList.remove("active");
                    installmentModalBlockList.forEach((block) => {
                        if (block !== null) {
                            block.classList.remove("active");
                            document.body.style.overflow = null;
                            document.body.style.marginRight = null;
                        }
                    });
                }
            });
        }
    };

    installmentModal();

    const buyersPhotos = () => {
        const BUYERS_PHOTOS_REQUEST_URL =
            "../assets/api/productDetail/buyersPhotos.json";
        const buyersPhotosSection = document.querySelector(".buyers-photos");
        const photosList = document.querySelector(".photos-list");
        const buyersGallery = document.querySelector(".buyers-gallery");
        const buyersGalleryWrap = document.querySelector(
            ".buyers-gallery__wrap"
        );
        const buyersGallerySwiper = document.querySelector(
            ".buyers-gallery__photo-wrap"
        );
        const buyersGallerySwiperThumbs = document.querySelector(
            ".buyers-gallery__thumbs-wrap"
        );
        const buyersGalleryBtnClose = document.querySelector(
            ".buyers-gallery__btn-close"
        );

        // Запрос на сервер
        const buyersPhotosRequest = async () => {
            try {
                const response = await fetch(BUYERS_PHOTOS_REQUEST_URL);

                if (response.ok) {
                    const data = await response.json();
                    /* если с сервера пришел ответ, то запускаем 
                    функцю рендера фото и свайпера*/
                    renderPhotos(data);
                } else {
                    console.error(`Ошибка HTTP: ${response.status}`);
                }
            } catch (error) {
                console.error("Ошибка при запросе:", error);
            }
        };

        buyersPhotosRequest();

        function renderPhotos(data) {
            const buyersPhotosList = data.buyersPhotos;

            /* если список не undefind и не пустой 
            показываем блок "Фото покупателей" */
            if (buyersPhotosList !== undefined && buyersPhotosList.length) {
                buyersPhotosSection.classList.remove("hidden");

                // Определение кол-ва в строке в зависимости от кол-ва фото в списке
                function defineAmountPhotoInRow(amount) {
                    return buyersPhotosList.length >= amount
                        ? amount
                        : buyersPhotosList.length;
                }

                // Устанавливаем кол-во фото preview в строке в заисимости от разрешения экрана (брейкпоинтов)
                function photoItemsPerMediaBreakpoints() {
                    // Для мобильных
                    if (window.innerWidth < mediaBreakpoints.mobile) {
                        return createItemElems(defineAmountPhotoInRow(4));
                    }
                    // Для планшетов
                    if (window.innerWidth < mediaBreakpoints.tablet) {
                        return createItemElems(defineAmountPhotoInRow(5));
                    }
                    // Для ноутбуков
                    if (window.innerWidth < mediaBreakpoints.desktopSmall) {
                        return createItemElems(defineAmountPhotoInRow(6));
                    }

                    // Для десктопов
                    return createItemElems(defineAmountPhotoInRow(8));
                }

                photoItemsPerMediaBreakpoints();

                // Создаем элементы в preview
                function createItemElems(ItemsAmount) {
                    for (let i = 0; i < ItemsAmount; i++) {
                        // Сосдаем обертку для фото в preview
                        const photoItem = document.createElement("li");
                        photoItem.classList.add("photos-list__item");
                        photosList.append(photoItem);

                        //  Вызываем функцию создания фото в preview
                        createImg(
                            photoItem,
                            "photos-list__img",
                            buyersPhotosList[i].link,
                            "buyer-photo"
                        );

                        // Создаем элемент скрытых фото
                        if (
                            i === ItemsAmount - 1 &&
                            data.buyersPhotos.length > ItemsAmount
                        ) {
                            const hiddenAmountElem =
                                document.createElement("div");
                            hiddenAmountElem.classList.add(
                                "photos-list__hidden-photos"
                            );

                            hiddenAmountElem.innerHTML =
                                "+" + (data.buyersPhotos.length - i - 1);

                            photoItem.append(hiddenAmountElem);
                        }
                    }
                }

                // Определяем элементы фото в DOM после их рендера
                const photosItemsList =
                    document.querySelectorAll(".photos-list__item");

                // Создаем слайды в свайпере
                for (let i = 0; i < buyersPhotosList.length; i++) {
                    const photoSwiperSlide = document.createElement("div");
                    // Вызываем функцию создания слайда
                    createSlide(
                        buyersGallerySwiper,
                        photoSwiperSlide,
                        "swiper-slide",
                        "buyers-gallery__photo-slide"
                    );
                    //  Вызываем функцию создания фото
                    createImg(
                        photoSwiperSlide,
                        "buyers-gallery__slide-img",
                        buyersPhotosList[i].link,
                        "couch"
                    );

                    const photoThumbsSlide = document.createElement("div");
                    // Вызываем функцию создания слайда в thumbs
                    createSlide(
                        buyersGallerySwiperThumbs,
                        photoThumbsSlide,
                        "swiper-slide",
                        "buyers-gallery__thumbs-slide"
                    );
                    //  Вызываем функцию создания фото в thumbs
                    createImg(
                        photoThumbsSlide,
                        "buyers-gallery__thumbs-img",
                        buyersPhotosList[i].link,
                        "couch"
                    );
                }

                // Создаем слайд
                function createSlide(target, elem, swiperClass, customClass) {
                    elem.classList.add(swiperClass);
                    elem.classList.add(customClass);
                    target.append(elem);
                }

                // Создаем фото
                function createImg(target, selector, link, alt) {
                    const imgElem = document.createElement("img");
                    imgElem.classList.add(selector);
                    imgElem.setAttribute("src", link);
                    imgElem.setAttribute("alt", alt);
                    target.append(imgElem);
                }

                photosItemsList.forEach((item, i) => {
                    if (item !== null) {
                        // Включение модалки со свайпером по клику на фото в preview
                        item.addEventListener("click", function () {
                            buyersGallery.classList.add("show");
                            document.body.style.overflow = "hidden";
                            swiperBuyersPhotoThumbs.slideTo(i);
                            swiperBuyersPhoto.slideTo(i);
                        });
                    }
                });

                // Закрытие модалки свайпера по клику вне блока свайпера
                if (buyersGallery !== null) {
                    buyersGallery.addEventListener("click", function () {
                        this.classList.remove("show");
                        document.body.style.overflow = null;
                    });
                }

                // Отмена всплытия события клика внутри блока свайпера
                if (buyersGalleryWrap !== null) {
                    buyersGalleryWrap.addEventListener("click", function (e) {
                        e.stopPropagation();
                    });
                }

                // Закрытие модалки свайпера по клику на кнопку "закрыть"
                if (buyersGalleryBtnClose !== null) {
                    buyersGalleryBtnClose.addEventListener(
                        "click",
                        function () {
                            buyersGallery.classList.remove("show");
                            document.body.style.overflow = null;
                        }
                    );
                }

                // Инициализация thumbs у свайпера
                const swiperBuyersPhotoThumbs = new Swiper(
                    ".buyers-gallery__thumbs",
                    {
                        spaceBetween: 20,
                        slidesPerView: 6,
                        freeMode: true,
                        watchSlidesProgress: true,
                        grabCursor: true,

                        // Responsive breakpoints
                        breakpoints: {
                            // when window width is >= 320px
                            320: {
                                slidesPerView: 4,
                                spaceBetween: 10,
                            },
                            // when window width is >= 768px
                            768: {
                                slidesPerView: 5,
                                spaceBetween: 20,
                            },
                            // when window width is >= 1024px
                            1024: {
                                slidesPerView: 6,
                            },
                        },
                    }
                );

                // Инициализация  свайпера
                const swiperBuyersPhoto = new Swiper(".buyers-gallery__photo", {
                    spaceBetween: 20,
                    grabCursor: true,
                    slidesPerView: 1,

                    navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                    },

                    thumbs: {
                        swiper: swiperBuyersPhotoThumbs,
                    },
                });
            }
        }
    };

    buyersPhotos();

    const swiperSimilarProducts = new Swiper(".similar-products", {
        slidesPerView: 1,
        speed: 1000,
        spaceBetween: 20,
        grabCursor: true,

        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },

        // Responsive breakpoints
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 2.1,
            },
            // when window width is >= 576px
            576: {
                slidesPerView: 2.5,
            },
            // when window width is >= 768px
            768: {
                slidesPerView: 3.2,
            },
            // when window width is >= 1024px
            1024: {
                slidesPerView: 4.2,
            },
            // when window width is >= 1920px
            1920: {
                slidesPerView: 6,
            },
        },
    });

    const swiperPreviewPhotoThumbs = new Swiper(".preview-gallery__thumbs", {
        spaceBetween: 20,
        slidesPerView: 6,
        freeMode: true,
        watchSlidesProgress: true,
        grabCursor: true,
    });

    const swiperPreviewPhoto = new Swiper(".preview-gallery__photo", {
        spaceBetween: 20,
        grabCursor: true,

        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        thumbs: {
            swiper: swiperPreviewPhotoThumbs,
        },
    });

    const swiperMobile = new Swiper(".preview-mobile__swiper", {
        slidesPerView: 1,
        speed: 1000,
        spaceBetween: 30,
        grabCursor: true,

        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
        },

        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },

        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
            },
            // when window width is >= 576px
            576: {
                spaceBetween: 45,
                slidesPerView: 1,
            },
            // when window width is >= 768px
            768: {
                slidesPerView: 1,
            },
        },
    });
});
