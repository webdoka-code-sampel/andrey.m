"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const citySelect = () => {
        const citySelectBlock = document.querySelectorAll(
            ".header-top__select"
        );
        const currentCityElem = document.querySelectorAll(
            ".header-top__current-select"
        );

        let currentCity = null;

        const setCurrentCity = () => {
            currentCityElem.forEach((item) => {
                item.innerHTML = currentCity;
            });
        };

        citySelectBlock.forEach((select) => {
            if (select !== null) {
                const selectBtn = select.querySelector(
                    ".header-top__select-btn"
                );
                const selectOptionsBlock = select.querySelector(
                    ".header-top__select-options"
                );
                const selectOptions = select.querySelectorAll(
                    ".header-top__select-option"
                );

                selectBtn.addEventListener("click", () => {
                    selectOptionsBlock.classList.toggle("active");
                });

                selectOptions.forEach((option) => {
                    option.addEventListener("click", () => {
                        selectOptionsBlock.classList.toggle("active");
                        currentCity = option.innerHTML;
                        setCurrentCity();
                    });
                });

                document.addEventListener("click", (e) => {
                    if (!e.target.closest(".header-top__select")) {
                        selectOptionsBlock.classList.remove("active");
                    }
                });
            }
        });
    };

    citySelect();

    const menuActions = () => {
        const menuBlock = document.querySelector(".header-menu");

        if (menuBlock !== null) {
            // Десктоп меню
            const menuBtn = menuBlock.querySelector(".header-menu__btn");
            const menu = menuBlock.querySelector(".menu");
            const menuItems = menu.querySelectorAll(".menu__item");
            const submenuList = menu.querySelectorAll(".submenu__list-wrap");
            // Мобильное меню
            const mobileMenu = menuBlock.querySelector(".mobile-menu");
            const mobileMenuBtnClose = mobileMenu.querySelectorAll(
                ".mobile-menu__btn-close"
            );
            const mobileMenuItems = mobileMenu.querySelectorAll(".menu__item");
            const mobileSubmenu = mobileMenu.querySelector(".submenu");
            const mobileSubmenuList = mobileMenu.querySelectorAll(
                ".submenu__list-wrap"
            );
            const submenuBtnBack = mobileMenu.querySelectorAll(
                ".submenu-header__btn-back"
            );
            const submenuTitle = mobileMenu.querySelector(
                ".submenu-header__title"
            );

            let currentMenuItemIndex = 0;

            let currentSubmenuTitle = null;

            if (window.innerWidth >= 1024) {
                menuBtn.addEventListener("click", () => {
                    menu.classList.toggle("active");
                    if (currentMenuItemIndex !== 0) {
                        submenuList[currentMenuItemIndex].classList.remove(
                            "active"
                        );
                        currentMenuItemIndex = 0;
                        submenuList[currentMenuItemIndex].classList.add(
                            "active"
                        );
                    } else {
                        currentMenuItemIndex = 0;
                        submenuList[currentMenuItemIndex].classList.add(
                            "active"
                        );
                    }
                });

                menuItems.forEach((item, idx) => {
                    if (item !== null) {
                        item.addEventListener("mouseenter", () => {
                            if (
                                submenuList[currentMenuItemIndex] !== undefined
                            ) {
                                submenuList[
                                    currentMenuItemIndex
                                ].classList.remove("active");
                            }
                            currentMenuItemIndex = idx;
                            console.log(submenuList[currentMenuItemIndex]);
                            if (
                                submenuList[currentMenuItemIndex] !== undefined
                            ) {
                                submenuList[currentMenuItemIndex].classList.add(
                                    "active"
                                );
                            }
                        });
                    }
                });

                document.addEventListener("click", (e) => {
                    if (!e.target.closest(".header-menu")) {
                        menu.classList.remove("active");
                    }
                });
            } else {
                const mobileMenuBtn = document.querySelector(
                    ".mobile-navbar__item--menu-btn"
                );

                menuBtn.addEventListener("click", () => {
                    mobileMenu.classList.add("show");
                    document.body.style.overflow = "hidden";
                });

                mobileMenuBtn.addEventListener("click", () => {
                    mobileMenu.classList.add("show");
                    document.body.style.overflow = "hidden";
                });

                mobileMenuBtnClose.forEach((btn) => {
                    btn.addEventListener("click", () => {
                        mobileSubmenu
                            .closest(".mobile-menu__block")
                            .classList.add("hidding");
                        mobileSubmenu.classList.remove("show");
                        mobileMenu.style.overflow = null;

                        setTimeout(() => {
                            mobileSubmenu
                                .closest(".mobile-menu__block")
                                .classList.remove("hidding");
                            mobileMenu.classList.remove("show");
                            document.body.style.overflow = null;
                        }, 200);
                    });
                });

                mobileMenuItems.forEach((item, idx) => {
                    item.addEventListener("click", () => {
                        currentSubmenuTitle =
                            item.querySelector(".menu__item-title").innerHTML;

                        submenuTitle.innerHTML = currentSubmenuTitle;
                        submenuTitle.setAttribute(
                            "href",
                            item
                                .querySelector(".menu__item-link")
                                .getAttribute("href")
                        );

                        console.log(
                            item
                                .querySelector(".menu__item-link")
                                .getAttribute("href")
                        );
                        mobileSubmenu.classList.add("show");
                        mobileSubmenuList[
                            currentMenuItemIndex
                        ].classList.remove("active");
                        currentMenuItemIndex = idx;
                        mobileSubmenuList[currentMenuItemIndex].classList.add(
                            "active"
                        );
                        mobileMenu.style.overflow = "hidden";
                    });
                });

                submenuBtnBack.forEach((btn) => {
                    btn.addEventListener("click", () => {
                        mobileSubmenu.classList.remove("show");
                        mobileMenu.style.overflow = null;
                    });
                });

                document.addEventListener("click", (e) => {
                    if (e.target.classList.contains("mobile-menu")) {
                        mobileSubmenu
                            .closest(".mobile-menu__block")
                            .classList.add("hidding");
                        mobileSubmenu.classList.remove("show");
                        mobileMenu.style.overflow = null;

                        setTimeout(() => {
                            mobileSubmenu
                                .closest(".mobile-menu__block")
                                .classList.remove("hidding");
                            mobileMenu.classList.remove("show");
                            document.body.style.overflow = null;
                        }, 200);
                    }
                });
            }
        }
    };

    menuActions();

    const searchActions = () => {
        const searchBlock = document.querySelector(".header-search__wrap");

        if (searchBlock !== null) {
            const searchInput = searchBlock.querySelector(
                ".header-search__input"
            );
            const presearch = searchBlock.querySelector(".presearch");
            const loupeIcon = searchBlock.querySelector(
                ".header-search__icon-search"
            );
            const clearInputBtn = searchBlock.querySelector(
                ".header-search__icon-clear"
            );
            const cardsFoundList = searchBlock.querySelector(
                ".presearch-found__list"
            );
            const oftenSearchList = searchBlock.querySelector(
                ".presearch-often__list"
            );
            const showAllResultsBtn = searchBlock.querySelector(
                ".presearch__show-all"
            );

            let requestText = "";

            loupeIcon.classList.add("show");

            let typingTimer; // Таймер для отслеживания времени между нажатиями клавиш
            const doneTypingInterval = 1000; // Таймаут в миллисекундах (1 секунда)

            // Функция, которая будет вызываться после окончания набора
            const sendRequest = async () => {
                try {
                    //запрос поиска
                    console.log("Отправка запроса:", requestText);
                    //для обращения к заглушке, в поле поиска нужно ввести "test" - имя заглушки
                    const response = await fetch(
                        `../assets/api/presearch/${requestText}.json`
                    );

                    if (response.ok) {
                        let data = await response.json();
                        console.log(data);
                        presearchActions(data);
                    } else {
                        console.error(`Ошибка HTTP: ${response.status}`);
                        showAllResultsBtn.classList.remove("show");
                    }
                } catch (error) {
                    console.error("Ошибка при запросе:", error);
                }
            };

            const presearchActions = (data) => {
                console.log(data.searchResponse);
                if (data.searchResponse.products.length) {
                    let products = data.searchResponse.products;
                    let oftenSearched = data.searchResponse.oftenSearched;

                    showAllResultsBtn.classList.add("show");

                    oftenSearched.oftenSearchedList.forEach((el) => {
                        let item = document.createElement("li");
                        item.classList.add("presearch-often__item");

                        item.innerHTML = `
                                <a class="presearch-often__link" href="#">
                                    <img
                                        class="presearch-often__icon"
                                        src="${oftenSearched.icon}"
                                        alt="search"
                                    />
                                    <span class="presearch-often__text">
                                        <span class="presearch-often__text--bold"
                                            >${el.strong}</span
                                        > ${el.regular}
                                    </span>
                                </a>
                            `;

                        oftenSearchList.appendChild(item);
                    });

                    products.forEach((product) => {
                        let card = document.createElement("div");
                        card.classList.add("presearch-found__card");

                        card.innerHTML = `
                            <a class="presearch-found__card-preview" href="#">
                                <span class="presearch-found__card-img">
                                    <img
                                        src="${product.img}"
                                        alt="product-preview"
                                    />
                                </span>
    
                                <span class="presearch-found__card-desc">
                                    <span class="presearch-found__card-title">
                                        ${product.name}
                                    </span>
    
                                    <span class="presearch-found__card-cost">
                                        <span>${product.price}</span>
                                        ₽
                                    </span>
                                </span>
                            </a>
    
                            <div class="presearch-found__user-btns">
                                <a class="presearch-found__user-icon" href="#">
                                    <svg
                                        width="30"
                                        height="30"
                                        viewBox="0 0 30 30"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            fill-rule="evenodd"
                                            clip-rule="evenodd"
                                            d="M9.11905 5.54924C6.56251 5.54924 4.47619 7.69918 4.47619 10.3689C4.47619 12.0645 5.44254 13.8133 6.96728 15.6298C9.45153 18.5893 12.3281 21.6821 15 24.2207C17.6719 21.6821 20.5485 18.5893 23.0327 15.6298C24.5575 13.8133 25.5238 12.0645 25.5238 10.3689C25.5238 7.69918 23.4375 5.54924 20.881 5.54924C19.7763 5.54924 18.764 5.94813 17.9661 6.61718C17.1242 7.32314 16.6818 7.83033 16.4359 8.14115C16.3725 8.22136 16.319 8.29248 16.27 8.35841C16.2614 8.36988 16.2523 8.38229 16.2426 8.39531C16.2043 8.44713 16.1588 8.5085 16.1199 8.55846C16.0778 8.61238 15.9827 8.73352 15.8542 8.84394C15.7248 8.95522 15.4155 9.18317 14.9619 9.17399C14.5138 9.16492 14.2161 8.92957 14.0958 8.82092C13.9737 8.71063 13.8827 8.59086 13.8424 8.53738C13.7781 8.45229 13.7439 8.40407 13.7115 8.35857C13.67 8.30005 13.6316 8.24603 13.5367 8.12382C13.2975 7.81605 12.8644 7.31361 12.0339 6.61718C11.236 5.94813 10.2237 5.54924 9.11905 5.54924ZM2 10.3689C2 6.30706 5.17966 3 9.11905 3C10.8155 3 12.3755 3.61574 13.5977 4.64063C14.212 5.15573 14.661 5.59852 14.9965 5.96841C15.3341 5.60029 15.7857 5.15774 16.4023 4.64063C17.6245 3.61574 19.1845 3 20.881 3C24.8203 3 28 6.30706 28 10.3689C28 13.0245 26.5192 15.375 24.9063 17.2966C22.2356 20.4782 19.1162 23.8172 16.2437 26.5049C15.5381 27.165 14.4619 27.165 13.7563 26.5049C10.8838 23.8172 7.76439 20.4782 5.09373 17.2966C3.48076 15.375 2 13.0245 2 10.3689Z"
                                            fill="#2B2D33"
                                        />
                                    </svg>
                                </a>
    
                                <a class="presearch-found__user-icon" href="#">
                                    <svg
                                        width="30"
                                        height="30"
                                        viewBox="0 0 30 30"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            fill-rule="evenodd"
                                            clip-rule="evenodd"
                                            d="M2.92768 3H1C0.447715 3 0 3.44772 0 4V4.5C0 5.05228 0.447716 5.5 1 5.5H2.305C2.68619 5.5 3.03422 5.71671 3.2024 6.0588L3.73201 7.13602C3.75704 7.18693 3.77769 7.23987 3.79374 7.29429L6.62199 16.8821C6.79775 17.4779 6.38076 18.0829 5.81937 18.3489C4.56013 18.9455 3.6873 20.2441 3.6873 21.75C3.6873 23.1561 4.44829 24.3815 5.57426 25.0234C5.94577 25.2352 6.14551 25.6974 6.14551 26.125C6.14551 27.1605 6.97094 28 7.98916 28C9.00738 28 9.83281 27.1605 9.83281 26.125C9.83281 25.8135 10.0531 25.5 10.3646 25.5H20.3629C20.6744 25.5 20.8947 25.8135 20.8947 26.125C20.8947 27.1605 21.7202 28 22.7384 28C23.7566 28 24.582 27.1605 24.582 26.125C24.582 25.8135 24.8023 25.5 25.1138 25.5H25.2567C25.5629 25.5 25.8111 25.2518 25.8111 24.9456V24C25.8111 23.4477 25.3634 23 24.8111 23H7.37461C6.69579 23 6.14551 22.4404 6.14551 21.75C6.14551 21.0596 6.69579 20.5 7.37459 20.5H22.5666C24.3242 20.5 25.8378 19.2384 26.1825 17.4854L27.9514 8.49029C28.2557 6.94331 27.0922 5.5 25.541 5.5H6.29871C5.91752 5.5 5.56949 5.28328 5.4013 4.9412L5.12636 4.38197C4.70996 3.53501 3.85878 3 2.92768 3ZM10.2658 18C9.8225 18 9.4321 17.7081 9.30667 17.2829L6.9468 9.28293C6.75782 8.64228 7.238 8 7.90594 8H24.3252C24.9549 8 25.4279 8.57506 25.3064 9.19296L23.772 16.9951C23.6571 17.5793 23.1526 18 22.5666 18H10.2658Z"
                                            fill="#2B2D33"
                                        />
                                    </svg>
                                </a>
                            </div>
                        `;

                        cardsFoundList.appendChild(card);
                    });
                } else {
                    showAllResultsBtn.classList.remove("show");
                }
            };

            // Функция, вызываемая при нажатии клавиши в поле поиска
            const keyPressed = () => {
                clearTimeout(typingTimer); // Сброс таймера при каждом нажатии клавиши

                // Если прошло достаточно времени с момента последнего нажатия клавиши, отправляем запрос
                typingTimer = setTimeout(() => {
                    sendRequest();
                    oftenSearchList.innerHTML = null;
                    cardsFoundList.innerHTML = null;
                }, doneTypingInterval);
            };

            searchInput.addEventListener("click", () => {
                presearch.classList.add("show");
            });

            searchInput.addEventListener("input", () => {
                requestText = searchInput.value;
                if (requestText) {
                    loupeIcon.classList.remove("show");
                    clearInputBtn.classList.add("show");
                } else {
                    loupeIcon.classList.add("show");
                    clearInputBtn.classList.remove("show");
                }

                // if (requestText.length >= 3) {
                //     presearch.classList.add("show");
                // }

                keyPressed();
                console.log("Введенный текст:" + requestText);
            });

            clearInputBtn.addEventListener("click", () => {
                searchInput.value = "";
                requestText = "";
                oftenSearchList.innerHTML = null;
                cardsFoundList.innerHTML = null;
                showAllResultsBtn.classList.remove("show");
                loupeIcon.classList.add("show");
                clearInputBtn.classList.remove("show");
                presearch.classList.remove("show");
                console.log("Введенный текст:" + requestText);
            });

            document.addEventListener("click", (e) => {
                if (!e.target.closest(".header-search__wrap")) {
                    presearch.classList.remove("show");
                }
            });
        }
    };

    searchActions();

    const callbackModal = () => {
        const callbackModalElem = document.querySelector(
            ".callback-modal__wrap"
        );
        const callbackBtn = document.querySelectorAll(".callback-btn");
        const callbackModalCloseBtn = callbackModalElem.querySelector(
            ".callback-modal__btn-close"
        );

        // Вычисляем ширину скроллбара
        const scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;

        callbackBtn.forEach((btn) => {
            btn.addEventListener("click", () => {
                callbackModalElem.classList.add("show");
                document.body.style.overflow = "hidden";
                document.body.style.marginRight = scrollbarWidth + "px";
            });
        });

        callbackModalCloseBtn.addEventListener("click", () => {
            callbackModalElem.classList.remove("show");
            document.body.style.overflow = null;
            document.body.style.marginRight = null;
        });

        document.addEventListener("click", (e) => {
            if (e.target.classList.contains("callback-modal__wrap")) {
                callbackModalElem.classList.remove("show");
                document.body.style.overflow = null;
                document.body.style.marginRight = null;
            }
        });
    };

    callbackModal();

    const headerFixed = () => {
        const headerBlock = document.querySelector(".header");

        if (headerBlock !== null) {
            if (window.innerWidth >= 1024) {
                const headerTop = document.querySelector(".header-top");
                const main = document.querySelector("main");
                const headerCategories =
                    document.querySelector(".header-categories");
                const headerCategoriesList = document.querySelector(
                    ".header-categories__list"
                );

                const headerTopHeight = headerTop.offsetHeight;
                const headerBlockHeight = headerBlock.offsetHeight;
                const headerCategoriesHeight = headerCategories.offsetHeight;
                headerCategoriesList.style.height =
                    headerCategoriesHeight + "px";

                window.addEventListener("scroll", () => {
                    // Проверяем, если прокрутка достигла 46px
                    if (window.scrollY >= headerTopHeight) {
                        headerBlock.classList.add("fixed");
                        main.style.marginTop =
                            headerBlockHeight + headerTopHeight + "px";
                        headerCategoriesList.removeAttribute("style");
                    } else {
                        // Удаляем класс 'active', если прокрутка меньше 46px
                        headerBlock.classList.remove("fixed");
                        main.style.marginTop = null;
                        headerCategoriesList.style.height =
                            headerCategoriesHeight + "px";
                    }
                });
            }
        }
    };

    headerFixed();

    const cardImgSwiper = () => {
        const catalogCardList = document.querySelectorAll(".catalog-card");

        if (catalogCardList.length) {
            catalogCardList.forEach((card) => {
                if (card !== null) {
                    const cardImgListWrap = card.querySelector(
                        ".catalog-card__img-wrap"
                    );
                    const cardImgList =
                        card.querySelectorAll(".catalog-card__img");
                    const slidebar = card.querySelector(
                        ".catalog-card__slidebar"
                    );

                    const slideEdgeWidth =
                        cardImgListWrap.offsetWidth / cardImgList.length;
                    let xPos;
                    let slidePosOver;
                    let currentSlide = 0;

                    cardImgList[currentSlide].classList.add("active");

                    if (window.innerWidth >= 1024) {
                        for (let i = 0; i < cardImgList.length; i++) {
                            const slidebarItem = document.createElement("span");
                            slidebarItem.classList.add(
                                "catalog-card__slidebar-item"
                            );
                            slidebar.appendChild(slidebarItem);
                        }

                        const slidebarItem = card.querySelectorAll(
                            ".catalog-card__slidebar-item"
                        );

                        slidebarItem[currentSlide].classList.add("active");

                        cardImgListWrap.addEventListener("mousemove", (e) => {
                            e.stopPropagation();

                            xPos =
                                e.clientX -
                                cardImgListWrap.getBoundingClientRect().left;
                            slidePosOver = Math.ceil(xPos / slideEdgeWidth) - 1;

                            if (slidePosOver >= 0) {
                                cardImgList[currentSlide].classList.remove(
                                    "active"
                                );
                                slidebarItem[currentSlide].classList.remove(
                                    "active"
                                );
                                currentSlide = slidePosOver;
                                cardImgList[currentSlide].classList.add(
                                    "active"
                                );
                                slidebarItem[currentSlide].classList.add(
                                    "active"
                                );
                                console.log("Текуший слайд:" + currentSlide);

                                if (cardImgList[currentSlide].dataset.src) {
                                    cardImgList[currentSlide].setAttribute(
                                        "src",
                                        cardImgList[currentSlide].dataset.src
                                    );

                                    cardImgList[currentSlide].removeAttribute(
                                        "data-src"
                                    );
                                }
                            }
                        });

                        cardImgListWrap.addEventListener("mouseleave", () => {
                            cardImgList[currentSlide].classList.remove(
                                "active"
                            );
                            slidebarItem[currentSlide].classList.remove(
                                "active"
                            );
                            currentSlide = 0;
                            cardImgList[currentSlide].classList.add("active");
                            slidebarItem[currentSlide].classList.add("active");
                            console.log("Текуший слайд:" + currentSlide);
                        });
                    }
                }
            });
        }
    };

    cardImgSwiper();

    const cooperationModal = () => {
        const cooperationBtn = document.querySelectorAll(".cooperation-btn");
        const cooperationModal = document.querySelector(".cooperation-modal");

        // Вычисляем ширину скроллбара
        const scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;

        cooperationBtn.forEach((btn) => {
            if (btn !== null) {
                btn.addEventListener("click", () => {
                    cooperationModal.classList.add("show");
                    document.body.style.overflow = "hidden";
                    document.body.style.marginRight = scrollbarWidth + "px";
                });
            }
        });

        if (cooperationModal !== null) {
            const closeModalBtn = cooperationModal.querySelectorAll(
                ".cooperation-modal__btn-close"
            );
            const modalWrap = cooperationModal.querySelector(
                ".cooperation-modal__wrap"
            );
            const sendSuccessElem =
                cooperationModal.querySelector(".send-success");
            const cooperationform =
                cooperationModal.querySelector(".cooperation-form");
            const textInputs = cooperationform.querySelectorAll(
                ".cooperation-form__input"
            );
            const selectItems = cooperationform.querySelectorAll(
                ".cooperation-select__item"
            );
            const requestCheckboxes = cooperationform.querySelectorAll(
                ".cooperation-form__item"
            );
            const submitBtn = cooperationform.querySelector(
                ".cooperation-form__submit-btn"
            );

            closeModalBtn.forEach((btn) => {
                if (btn !== null) {
                    btn.addEventListener("click", () => {
                        cooperationModal.classList.remove("show");
                        document.body.style.overflow = null;
                        document.body.style.marginRight = null;
                        sendSuccessElem.classList.remove("show");
                        cooperationModal.style.display = "";
                        modalWrap.classList.remove("hidden");
                        selectItems.forEach((item) => {
                            if (item !== null) {
                                item.classList.remove("active");
                            }
                        });
                    });
                }
            });

            requestCheckboxes.forEach((item) => {
                if (item !== null) {
                    const inputCheckbox = item.querySelector(
                        ".cooperation-form__input-checkbox"
                    );
                    const checkbox = item.querySelector(
                        ".cooperation-form__checkbox"
                    );

                    if (checkbox !== null) {
                        checkbox.addEventListener("click", () => {
                            if (inputCheckbox.checked) {
                                item.classList.remove("active");
                                inputCheckbox.checked = false;
                            } else {
                                item.classList.add("active");
                                inputCheckbox.checked = true;
                            }
                        });
                    }
                }
            });

            selectItems.forEach((item) => {
                if (item !== null) {
                    const selectBtn = item.querySelector(
                        ".cooperation-select__btn"
                    );
                    const currentOptions = item.querySelector(
                        ".cooperation-select__current"
                    );
                    const selectOptions = item.querySelectorAll(
                        ".cooperation-select__options-item"
                    );

                    if (selectBtn !== null) {
                        selectBtn.addEventListener("click", () => {
                            item.classList.toggle("active");
                        });
                    }

                    selectOptions.forEach((option) => {
                        if (option !== null) {
                            const inputRadio = option.querySelector(
                                ".cooperation-select__options-radio"
                            );
                            const optionTitle = option.querySelector(
                                ".cooperation-select__options-title"
                            );

                            option.addEventListener("click", () => {
                                if (!inputRadio.checked) {
                                    const optionsListWrap =
                                        option.parentElement;
                                    const optionsList =
                                        optionsListWrap.querySelectorAll(
                                            ".cooperation-select__options-item"
                                        );
                                    optionsList.forEach((el) => {
                                        el.classList.remove("active");
                                    });

                                    option.classList.add("active");
                                    inputRadio.checked = true;
                                    currentOptions.innerHTML =
                                        optionTitle.innerHTML;
                                }

                                item.classList.remove("active");
                            });
                        }
                    });
                }
            });

            if (submitBtn !== null) {
                submitBtn.addEventListener("click", () => {
                    textInputs.forEach((input) => {
                        if (input.required && !input.value) {
                            input.classList.add("error");
                        }
                    });
                });
            }

            textInputs.forEach((input) => {
                if (input.required) {
                    input.addEventListener("input", () => {
                        if (input.value && input.classList.contains("error")) {
                            input.classList.remove("error");
                        }
                    });
                }
            });

            if (cooperationform !== null) {
                cooperationform.addEventListener("submit", (e) => {
                    e.preventDefault();
                    cooperationform.reset();

                    requestCheckboxes.forEach((item) => {
                        if (item !== null) {
                            item.classList.remove("active");
                        }
                    });

                    selectItems.forEach((item) => {
                        if (item !== null) {
                            const currentOptions = item.querySelector(
                                ".cooperation-select__current"
                            );
                            const selectOptions = item.querySelectorAll(
                                ".cooperation-select__options-item"
                            );

                            item.classList.remove("active");

                            selectOptions.forEach((option, i) => {
                                const optionTitle = option.querySelector(
                                    ".cooperation-select__options-title"
                                );

                                option.classList.remove("active");
                                if (i === 0) {
                                    option.classList.add("active");
                                    currentOptions.innerHTML =
                                        optionTitle.innerHTML;
                                }
                            });
                        }
                    });

                    modalWrap.classList.add("hidden");
                    sendSuccessElem.classList.add("show");
                    cooperationModal.style.display = "flex";
                });
            }

            document.addEventListener("click", (e) => {
                if (e.target.classList.contains("cooperation-modal")) {
                    cooperationModal.classList.remove("show");
                    modalWrap.classList.remove("hidden");
                    document.body.style.overflow = null;
                    document.body.style.marginRight = null;
                    sendSuccessElem.classList.remove("show");
                    cooperationModal.style.display = "";
                    selectItems.forEach((item) => {
                        if (item !== null) {
                            item.classList.remove("active");
                        }
                    });
                }
            });
        }
    };

    cooperationModal();

    const authentication = () => {
        const authenticationModal = document.querySelector(
            ".authentication-modal"
        );
        const authenticationOpenBtn = document.querySelectorAll(
            ".authentication-btn"
        );

        const scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;

        authenticationOpenBtn.forEach((btn) => {
            if (btn !== null) {
                btn.addEventListener("click", (e) => {
                    e.preventDefault();
                    authenticationModal.classList.add("show");
                    document.body.style.overflow = "hidden";
                    document.body.style.marginRight = scrollbarWidth + "px";
                });
            }
        });

        if (authenticationModal !== null) {
            // authorizationBlock elems
            const authorizationBlock =
                authenticationModal.querySelector(".authorization");
            const authorizationForm = authorizationBlock.querySelector(
                ".authorization-form"
            );
            const registrationLink = authorizationBlock.querySelectorAll(
                ".authorization-form__link"
            )[0];
            const passwordRecoveryLink = authorizationBlock.querySelectorAll(
                ".authorization-form__link"
            )[1];
            // registrationBlock elems
            const registrationBlock =
                authenticationModal.querySelector(".registration");
            const registrationForm =
                registrationBlock.querySelector(".registration-form");
            const registrationSuccess = registrationBlock.querySelector(
                ".registration-success"
            );
            const authorizationLink = registrationBlock.querySelector(
                ".registration-form__link"
            );
            //passwordRecoveryBlock elems
            const passwordRecoveryBlock = authenticationModal.querySelector(
                ".password-recovery "
            );
            const passwordRecoveryForm =
                passwordRecoveryBlock.querySelector(".recovery-form");
            const passwordRecoverySuccess =
                passwordRecoveryBlock.querySelector(".recovery-success");
            const authenticationCloseBtn = authenticationModal.querySelectorAll(
                ".authentication-close"
            );

            const RE_EMAIL =
                /^(([^&lt;&gt;()\[\]\\.,;:\s@"]+(\.[^&lt;&gt;()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            const validateEmail = (email) => {
                return RE_EMAIL.test(String(email).toLowerCase());
            };

            const authenticationMoadlClose = () => {
                authenticationModal.classList.remove("show");
                authorizationBlock.classList.remove("hidden");
                registrationBlock.classList.add("hidden");
                registrationForm.classList.remove("hidden");
                registrationSuccess.classList.remove("show");
                passwordRecoveryBlock.classList.add("hidden");
                passwordRecoveryForm.classList.remove("hidden");
                passwordRecoverySuccess.classList.remove("show");
                document.body.style.overflow = null;
                document.body.style.marginRight = null;
            };

            authenticationCloseBtn.forEach((btn) => {
                if (btn !== null) {
                    btn.addEventListener("click", () => {
                        authenticationMoadlClose();
                    });
                }
            });

            if (registrationLink !== null) {
                registrationLink.addEventListener("click", () => {
                    authorizationBlock.classList.add("hidden");
                    registrationBlock.classList.remove("hidden");
                });
            }

            if (passwordRecoveryLink !== null) {
                passwordRecoveryLink.addEventListener("click", () => {
                    authorizationBlock.classList.add("hidden");
                    passwordRecoveryBlock.classList.remove("hidden");
                });
            }

            if (authorizationForm !== null) {
                const authorizationFormInputs =
                    authorizationForm.querySelectorAll(
                        ".authorization-form__input"
                    );
                const submitBtn = authorizationForm.querySelector(
                    ".authorization-form__submit-btn"
                );

                authorizationFormInputs.forEach((input) => {
                    if (input !== null) {
                        input.addEventListener("input", () => {
                            if (input.value) {
                                input.classList.remove("error");
                            } else {
                                input.classList.add("error");
                            }
                        });
                    }
                });

                submitBtn.addEventListener("click", () => {
                    authorizationFormInputs.forEach((input) => {
                        if (input !== null) {
                            if (!input.value) {
                                input.classList.add("error");
                            }
                        }
                    });
                });

                authorizationForm.addEventListener("submit", (e) => {
                    e.preventDefault();
                    authorizationForm.reset();
                    authenticationModal.classList.remove("show");
                });
            }

            if (authorizationLink !== null) {
                authorizationLink.addEventListener("click", () => {
                    registrationBlock.classList.add("hidden");
                    authorizationBlock.classList.remove("hidden");
                });
            }

            if (registrationForm !== null) {
                const registrationFormInputs =
                    registrationForm.querySelectorAll(
                        ".registration-form__input"
                    );
                const emailInput = registrationForm.querySelector(
                    'input[type="email"]'
                );
                const passwordInput = registrationForm.querySelectorAll(
                    'input[type="password"]'
                )[0];
                const passwordInputRepeat = registrationForm.querySelectorAll(
                    'input[type="password"]'
                )[1];
                const submitBtn = registrationForm.querySelector(
                    ".registration-form__submit-btn"
                );

                let isRegistrationFormValid = false;

                registrationFormInputs.forEach((input) => {
                    if (input !== null) {
                        input.addEventListener("input", () => {
                            registrationFormInputs.forEach((el) => {
                                if (el !== null) {
                                    if (!el.value) {
                                        el.classList.add("error");
                                    } else {
                                        el.classList.remove("error");
                                    }
                                }
                            });

                            if (
                                passwordInput.value &&
                                passwordInputRepeat.value &&
                                passwordInput.value ===
                                    passwordInputRepeat.value
                            ) {
                                passwordInput.classList.remove("error");
                                passwordInputRepeat.classList.remove("error");
                            } else {
                                passwordInput.classList.add("error");
                                passwordInputRepeat.classList.add("error");
                            }

                            if (validateEmail(emailInput.value)) {
                                emailInput.classList.remove("error");
                            } else {
                                emailInput.classList.add("error");
                            }
                        });
                    }
                });

                submitBtn.addEventListener("click", () => {
                    if (
                        passwordInput.value &&
                        passwordInputRepeat.value &&
                        passwordInput.value !== passwordInputRepeat.value
                    ) {
                        passwordInput.classList.add("error");
                        passwordInputRepeat.classList.add("error");
                    } else {
                        passwordInput.classList.remove("error");
                        passwordInputRepeat.classList.remove("error");
                    }

                    if (!validateEmail(emailInput.value)) {
                        emailInput.classList.add("error");
                    }

                    registrationFormInputs.forEach((input) => {
                        if (input !== null) {
                            if (!input.value) {
                                input.classList.add("error");
                            }
                        }
                    });

                    if (
                        validateEmail(emailInput.value) &&
                        passwordInput.value === passwordInputRepeat.value
                    ) {
                        isRegistrationFormValid = true;
                    } else {
                        isRegistrationFormValid = false;
                    }
                });

                registrationForm.addEventListener("submit", (e) => {
                    e.preventDefault();

                    if (isRegistrationFormValid) {
                        console.log(isRegistrationFormValid);
                        registrationForm.reset();
                        registrationForm.classList.add("hidden");
                        registrationSuccess.classList.add("show");
                        passwordInput.classList.remove("error");
                        passwordInputRepeat.classList.remove("error");
                    }
                });
            }

            if (passwordRecoveryForm !== null) {
                const emailInput = passwordRecoveryForm.querySelector(
                    'input[type="email"]'
                );
                const submitBtn = passwordRecoveryForm.querySelector(
                    ".recovery-form__submit-btn"
                );

                let passwordRecoveryFormValid = false;

                if (emailInput !== null) {
                    emailInput.addEventListener("input", () => {
                        if (!validateEmail(emailInput.value)) {
                            emailInput.classList.add("error");
                        } else {
                            emailInput.classList.remove("error");
                        }
                    });
                }

                submitBtn.addEventListener("click", () => {
                    if (!validateEmail(emailInput.value)) {
                        emailInput.classList.add("error");
                    } else {
                        passwordRecoveryFormValid = true;
                    }
                });

                passwordRecoveryForm.addEventListener("submit", (e) => {
                    e.preventDefault();
                    passwordRecoveryForm.reset();
                    passwordRecoveryForm.classList.add("hidden");
                    passwordRecoverySuccess.classList.add("show");
                });
            }

            document.addEventListener("click", (e) => {
                if (e.target.classList.contains("authentication-modal")) {
                    authenticationMoadlClose();
                }
            });
        }
    };

    authentication();

    const accountPopupActions = () => {
        const accountBtns = document.querySelectorAll(".account-btn");

        accountBtns.forEach((btn) => {
            if (btn !== null) {
                if (window.innerWidth >= 768) {
                    const accountSectionsPopup =
                        document.querySelector(".account-sections");
                    const accountSectionsLink = btn.querySelectorAll(
                        ".account-sections__link"
                    );

                    btn.addEventListener("click", () => {
                        accountSectionsPopup.classList.toggle("show");
                    });

                    // accountSectionsPopup.addEventListener("mouseleave", () => {
                    //     accountSectionsPopup.classList.remove("show");
                    // });

                    accountSectionsLink.forEach((link) => {
                        if (link !== null) {
                            link.addEventListener("click", () => {
                                accountSectionsPopup.classList.remove("show");
                            });
                        }
                    });

                    document.addEventListener("click", (e) => {
                        if (!e.target.closest(".account-btn")) {
                            accountSectionsPopup.classList.remove("show");
                        }
                    });
                } else {
                    const accountModal =
                        document.querySelector(".account-modal");

                    if (accountModal !== null) {
                        const closeBtn = accountModal.querySelector(
                            ".account-modal__close-btn"
                        );

                        btn.addEventListener("click", () => {
                            accountModal.classList.add("show");
                            document.body.style.overflow = "hidden";
                        });

                        closeBtn.addEventListener("click", () => {
                            accountModal.classList.remove("show");
                            document.body.style.overflow = "";
                        });

                        window.addEventListener("click", (e) => {
                            if (e.target === accountModal) {
                                accountModal.classList.remove("show");
                                document.body.style.overflow = "";
                            }
                        });
                    }
                }
            }
        });
    };

    accountPopupActions();

    const vacancyActions = () => {
        const resumeBtn = document.querySelectorAll(".resume-btn");
        const resumeModal = document.querySelector(".resume-modal");
        const resumeForm = document.querySelectorAll(".resume-form");
        const resumeModalClose = document.querySelector(
            ".resume-form__close-wrap"
        );
        const resumeSuccess = document.querySelector(".resume-success");
        const resumeSuccessBtnClose = document.querySelector(
            ".resume-success__close"
        );

        const scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;

        const resumeModalOpen = () => {
            resumeModal.classList.add("show");
            document.body.style.overflow = "hidden";
            document.body.style.marginRight = scrollbarWidth + "px";
        };

        resumeBtn.forEach((btn) => {
            if (btn !== null) {
                btn.addEventListener("click", () => {
                    resumeModalOpen();
                });
            }
        });

        if (resumeModalClose !== null) {
            resumeModalClose.addEventListener("click", () => {
                resumeModal.classList.remove("show");
                document.body.style.overflow = null;
                document.body.style.marginRight = null;
            });
        }

        if (resumeSuccessBtnClose !== null) {
            resumeSuccessBtnClose.addEventListener("click", () => {
                resumeModal
                    .querySelector(".resume-form")
                    .classList.remove("hidden");
                resumeSuccess.classList.remove("show");
                resumeModal.classList.remove("show");
                document.body.style.overflow = null;
                document.body.style.marginRight = null;
            });
        }

        // Отправка формы
        const sendForm = async (formData) => {
            try {
                const response = await fetch("https://...", {
                    method: "POST",
                    body: formData,
                });

                if (response.ok) {
                    const result = await response.json();
                    console.log("Server Response:", result);
                } else {
                    console.error(`Ошибка HTTP: ${response.status}`);
                }
            } catch (error) {
                console.error("Ошибка при запросе:", error);
            }
        };

        resumeForm.forEach((form) => {
            if (form !== null) {
                const nameInput = form.querySelector('input[name="name"]');
                const phoneInput = form.querySelector('input[name="phone"]');
                const loadBtn = form.querySelector(
                    ".resume-form__attachment-btn"
                );
                const loadInput = form.querySelector(
                    ".resume-form__attachment-input"
                );
                const loadDelBtn = form.querySelector(
                    ".resume-form__attachment-del"
                );
                const attachmentBox = form.querySelector(
                    ".resume-form__attachment"
                );
                const attachmentFileWrapper = form.querySelector(
                    ".resume-form__attachment-file"
                );

                let isFormValid = false;

                const validateForm = () => {
                    if (nameInput.value && phoneInput.value) {
                        isFormValid = true;
                        nameInput.classList.remove("error");
                        phoneInput.classList.remove("error");
                        console.log(isFormValid);
                    } else {
                        isFormValid = false;
                        console.log(isFormValid);

                        if (!nameInput.value) {
                            nameInput.classList.add("error");
                        } else {
                            nameInput.classList.remove("error");
                        }

                        if (!phoneInput.value) {
                            phoneInput.classList.add("error");
                        } else {
                            phoneInput.classList.remove("error");
                        }
                    }
                };

                nameInput.addEventListener("input", () => {
                    validateForm();
                });

                phoneInput.addEventListener("input", () => {
                    validateForm();
                });

                loadBtn.addEventListener("click", () => {
                    loadInput.click();
                });

                const updateLoadList = () => {
                    const files = loadInput.files;
                    const file = files[0];
                    const lastItem = loadDelBtn;
                    attachmentFileWrapper.innerHTML = "";

                    if (files.length) {
                        attachmentBox.classList.add("active");

                        const elem = document.createElement("div");
                        elem.className = "resume-form__attachment-item";
                        elem.textContent = file.name;

                        // Вставляем элемент удаления списка файлов
                        attachmentFileWrapper.appendChild(lastItem);

                        // Вставляем новый элемент перед последним
                        attachmentFileWrapper.insertBefore(elem, lastItem);
                    } else {
                        attachmentBox.classList.remove("active");
                    }
                };

                loadInput.addEventListener("change", () => {
                    updateLoadList();
                });

                loadDelBtn.addEventListener("click", () => {
                    loadInput.value = "";
                    updateLoadList();
                });

                form.addEventListener("submit", (e) => {
                    e.preventDefault();
                    validateForm();

                    if (isFormValid) {
                        // Собираем данные формы
                        const formData = new FormData(form);
                        sendForm(formData);
                        form.reset();
                        resumeModalOpen();
                        resumeModal
                            .querySelector(".resume-form")
                            .classList.add("hidden");
                        resumeSuccess.classList.add("show");
                        updateLoadList();
                        isFormValid = false;
                    }
                });
            }
        });
    };

    vacancyActions();

    const dolyameAccordeon = () => {
        const dolyameItemList = document.querySelectorAll(".dolyame-item");

        dolyameItemList.forEach((item, idx) => {
            if (item !== null) {
                const itemHeader = item.querySelector(".dolyame-item__header");
                const itemBasement = item.querySelector(
                    ".dolyame-item__basement"
                );

                // Делаем открытым первый магазин в списке
                if (item.classList.contains("active")) {
                    item.classList.add("active");
                    itemBasement.style.maxHeight =
                        itemBasement.scrollHeight + "px";
                }

                if (itemHeader !== null) {
                    itemHeader.addEventListener("click", () => {
                        item.classList.toggle("active");

                        if (!itemBasement.style.maxHeight) {
                            itemBasement.style.maxHeight =
                                itemBasement.scrollHeight + "px";
                        } else {
                            itemBasement.style.maxHeight = null;
                        }
                    });
                }
            }
        });
    };

    dolyameAccordeon();
});
