"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const data = {
        // CommonData
        body: document.body,
        header: document.querySelector(".header"),
        headerWrapper: document.querySelector(".header__wrap"),
        main: document.querySelector("main"),
        footer: document.querySelector(".footer"),
        callbackBtn: document.querySelectorAll(".callback-btn"),
        callbackModal: document.querySelector(".callback-modal"),
        callbackModalWrap: document.querySelector(".callback-modal__wrap"),
        callbackFormWrap: document.querySelector(".callback-modal__form-wrap"),
        callbackForm: document.querySelectorAll(".callback-form"),
        callbackModalBtnClose: document.querySelectorAll(
            ".callback-modal__btn-close"
        ),
        callbackSuccess: document.querySelector(".callback-success"),
        menuBlock: document.querySelector(".menu-block"),
        menuBtn: document.querySelectorAll(".menu-btn"),
        preloader: document.querySelector(".preloader"),
        mediaFiles: document.querySelectorAll("img"),
        preloaderPercentValue: document.querySelector(
            ".preloader-percent__value"
        ),
        preloaderProgressBar: document.querySelector(
            ".preloader-progress__bar"
        ),
        cookieBlock: document.querySelector(".cookie-block"),
        cookieAcceptBtn: document.querySelector(".cookie-block__accept"),
        agreementModal: document.querySelector(".agreement"),
        agreementWrapper: document.querySelector(".agreement__wrap"),
        agreementLinkList: document.querySelectorAll(".agreement-link"),
        agreementBtnClose: document.querySelector(".agreement__btn-close"),
        upBtn: document.querySelector(".footer__btn-title"),

        //Main Page
        previewSection: document.querySelector(".preview"),
        previewImageBlock: document.querySelector(".preview-image"),
        previewImage: document.querySelector(".preview-image__wrap"),
        previewFirstSmallImage: document.querySelectorAll(
            ".preview-image__small-img"
        )[0],
        previewSecondSmallImage: document.querySelectorAll(
            ".preview-image__small-img"
        )[1],
        previewThirdSmallImage: document.querySelectorAll(
            ".preview-image__small-img"
        )[2],
        teamLineUp: document.querySelectorAll(".team-item"),
        teamImage: document.querySelector(".team-image"),
        teamLineUpBlock: document.querySelector(".team__wrap"),
        teamSmallImage: document.querySelector(".team-image__img"),
        solutionBlock: document.querySelector(".solution__wrap"),
        solutionImage: document.querySelector(".solution-image"),
        solutionSmallImage: document.querySelector(".solution-image__img"),
        expertCallBlock: document.querySelectorAll(".consultation__wrap")[0],
        expertCallImage: document.querySelectorAll(".consultation-image")[0],
        consultationBlock: document.querySelectorAll(".consultation__wrap")[1],
        consultationImage: document.querySelectorAll(".consultation-image")[1],
        reasonsNote: document.querySelector(".reasons-item__note-elem"),
        reasonsTooltip: document.querySelector(".reasons-item__tooltip"),
        servicesList: document.querySelectorAll(".services-item"),
        casesSlideList: document.querySelectorAll(".cases-slide"),
        swiperBtnPrevList: document.querySelectorAll(".cases-nav__btn-prev"),
        swiperBtnNextList: document.querySelectorAll(".cases-nav__btn-next"),
        currentSlideList: document.querySelectorAll(
            ".cases-nav__pagination-current"
        ),
        totalSlideList: document.querySelectorAll(
            ".cases-nav__pagination-total"
        ),
        swiperPagination: document.querySelectorAll(".cases-img__pagination"),
        priceDescElem: document.querySelectorAll(".price-desc__elem"),

        // 404 Page
        notFoundKeyLogo: document.querySelector(".notfound-logo__img"),

        // Вычисляемые данные
        scrollPosition: null,
        scrollbarWidth: null,
        headerHeight: null,
        footerHeight: null,
        imgTotalLoaded: 0,
        percentDisplay: 0,
        currentCasesSlide: 0,

        // Состояния
        isPreloaderActive: true,

        // Данные параллакса блоков
        previewBlockParallax: {
            isIntersecting: false,
            currentPos: 0,
            scrolledPercent: 0,
            borderRadius: null,
            previewImageHeight: null,
            previewImagePos: null,
        },
        teamLineUpParallax: {
            isIntersecting: false,
            currentPos: 0,
        },
        solutionBlockParallax: {
            isIntersecting: false,
            currentPos: 0,
        },
        expertCallBlockParallax: {
            isIntersecting: false,
            currentPos: 0,
        },
        consultationBlockParallax: {
            isIntersecting: false,
            currentPos: 0,
        },

        // Размеры экранов
        desktopSize: 1440,
        tabletSize: 1024,
        mobileSize: 768,
    };

    const setData = () => {
        // Высота Хедера
        data.headerHeight = data.header.clientHeight;
        // Добавляем margin-top мэину в зависимости от высоты хедера
        data.main.style.marginTop = data.headerHeight + "px";
        // Высота Футера
        if (data.footer !== null) {
            data.footerHeight = data.footer.clientHeight;
        }
        // Текущий скролл
        data.scrollPosition = document.documentElement.scrollTop;

        // Ширина скроллбара
        data.scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;
        if (window.innerWidth >= 1024) {
            data.previewBlockParallax.borderRadius = 50;
        } else {
            data.previewBlockParallax.borderRadius = 20;
        }
        // Высота preview блка
        data.previewBlockParallax.previewImageHeight =
            data.previewImageBlock.clientHeight;
        // Начальное смещение изображения preview
        data.previewBlockParallax.previewImagePos = -(
            data.previewBlockParallax.previewImageHeight / 2
        );

        // Если после загрузки страницы скролл за границами блока preview
        if (
            data.scrollPosition >
            data.previewBlockParallax.previewImageHeight * 2
        ) {
            data.previewBlockParallax.currentPos =
                data.previewBlockParallax.previewImageHeight * 2;
            data.previewBlockParallax.scrolledPercent = 100;
            data.previewImage.style.borderRadius = "0% 0% 0% 0%";
            data.previewSection.classList.add("scroll");
        }
        data.previewImage.style.transform =
            "translateY(" + data.previewBlockParallax.previewImagePos + "px)";
    };

    setData();

    // События
    const bodyScrollHide = () => {
        data.body.style.overflow = "hidden";
        data.body.style.marginRight = data.scrollbarWidth + "px";
        data.headerWrapper.style.marginRight = data.scrollbarWidth + "px";
    };

    const bodyScrollVisible = () => {
        data.body.style.overflow = null;
        data.body.style.marginRight = null;
        data.headerWrapper.style.marginRight = null;
    };

    // Прячем скроллбар и отменяем скролл старницы пока активен прелоадер
    if (data.isPreloaderActive) {
        bodyScrollHide();
    }

    // Вычисление загрузки страницы
    const imgLoad = () => {
        data.imgTotalLoaded++;
        data.percentDisplay =
            ((100 / data.mediaFiles.length) * data.imgTotalLoaded) << 0;

        data.preloaderPercentValue.innerText = data.percentDisplay;
        data.preloaderProgressBar.style.width = data.percentDisplay + "%";

        if (data.imgTotalLoaded === data.mediaFiles.length) {
            data.isPreloaderActive = false;
            data.preloader.classList.remove("show");
            bodyScrollVisible();
        }
    };

    // Отслежиыание загрузки изображения
    for (let i = 0; i < data.mediaFiles.length; i++) {
        let imgClone = new Image();
        imgClone.onload = imgLoad;
        imgClone.onerror = imgLoad;
        imgClone.src = data.mediaFiles[i].src;
    }

    // Прячем хедер
    const headerHidding = () => {
        if (data.scrollPosition >= data.headerHeight) {
            data.header.classList.add("scrolled");
        }

        if (data.scrollPosition === 0) {
            data.header.classList.remove("scrolled");
        }
    };

    // Направление скролла
    const scrollDirection = (prevPosition) => {
        if (data.scrollPosition > prevPosition) {
            data.header.classList.remove("up");
            // data.scrollDirection = "down";

            if (data.previewBlockParallax.isIntersecting) {
                data.previewSection.classList.add("scroll");

                data.previewBlockParallax.currentPos =
                    window.scrollY -
                    data.previewImageBlock.offsetTop +
                    data.previewImageBlock.clientHeight;

                if (data.previewBlockParallax.scrolledPercent <= 100) {
                    data.previewBlockParallax.scrolledPercent = Math.round(
                        ((window.scrollY -
                            data.previewImageBlock.offsetTop +
                            data.previewImageBlock.clientHeight) /
                            (data.previewImageBlock.clientHeight * 2)) *
                            100
                    );
                }

                if (window.innerWidth >= data.mobileSize) {
                    if (data.previewBlockParallax.scrolledPercent <= 50) {
                        data.previewImage.style.borderRadius = `${
                            data.previewBlockParallax.borderRadius -
                            data.previewBlockParallax.scrolledPercent
                        }% ${
                            data.previewBlockParallax.borderRadius -
                            data.previewBlockParallax.scrolledPercent
                        }% 0% 0%`;
                    }
                }

                data.previewImage.style.transform =
                    "translateY(" +
                    (data.previewBlockParallax.previewImagePos +
                        data.previewBlockParallax.currentPos * 0.4) +
                    "px)";

                data.previewSecondSmallImage.style.transform =
                    "translateY(-" +
                    data.previewBlockParallax.currentPos * 0.3 +
                    "px)";

                data.previewThirdSmallImage.style.transform =
                    "translateY(-" +
                    data.previewBlockParallax.currentPos * 0.3 +
                    "px)";
            }

            if (data.teamLineUpParallax.isIntersecting) {
                data.teamLineUpParallax.currentPos =
                    window.scrollY -
                    data.teamImage.offsetTop +
                    data.teamImage.clientHeight;

                if (data.teamLineUpParallax.currentPos >= 0) {
                    data.teamLineUpBlock.style.transform =
                        "translateY(" +
                        data.teamLineUpParallax.currentPos * 0.2 +
                        "px)";
                    data.teamSmallImage.style.transform =
                        "translateY(-" +
                        data.teamLineUpParallax.currentPos * 0.4 +
                        "px)";
                }
            }

            if (data.solutionBlockParallax.isIntersecting) {
                data.solutionBlockParallax.currentPos =
                    window.scrollY -
                    data.solutionImage.offsetTop +
                    data.solutionImage.clientHeight;

                if (data.solutionBlockParallax.currentPos >= 0) {
                    data.solutionBlock.style.transform =
                        "translateY(" +
                        data.solutionBlockParallax.currentPos * 0.4 +
                        "px)";
                    data.solutionSmallImage.style.transform =
                        "translateY(-" +
                        data.solutionBlockParallax.currentPos * 0.4 +
                        "px)";
                }
            }

            if (data.expertCallBlockParallax.isIntersecting) {
                data.expertCallBlockParallax.currentPos =
                    window.scrollY -
                    data.expertCallImage.offsetTop +
                    data.expertCallImage.clientHeight;

                if (data.expertCallBlockParallax.currentPos >= 0) {
                    data.expertCallBlock.style.transform =
                        "translateY(" +
                        data.expertCallBlockParallax.currentPos * 0.2 +
                        "px)";
                }
            }

            if (data.consultationBlockParallax.isIntersecting) {
                data.consultationBlockParallax.currentPos =
                    window.scrollY -
                    data.consultationImage.offsetTop +
                    data.consultationImage.clientHeight;

                if (data.consultationBlockParallax.currentPos >= 0) {
                    data.consultationBlock.style.transform =
                        "translateY(" +
                        data.consultationBlockParallax.currentPos * 0.2 +
                        "px)";
                }
            }
        } else {
            data.header.classList.add("up");
            // data.scrollDirection = "up";

            if (data.previewBlockParallax.isIntersecting) {
                data.previewBlockParallax.currentPos =
                    data.previewBlockParallax.currentPos -
                    (prevPosition - window.scrollY);

                if (data.previewBlockParallax.scrolledPercent > 0) {
                    data.previewBlockParallax.scrolledPercent = Math.round(
                        ((window.scrollY -
                            data.previewImageBlock.offsetTop +
                            data.previewImageBlock.clientHeight) /
                            (data.previewImageBlock.clientHeight * 2)) *
                            100
                    );
                }

                if (window.innerWidth >= data.mobileSize) {
                    if (data.previewBlockParallax.scrolledPercent <= 50) {
                        data.previewImage.style.borderRadius = `${
                            data.previewBlockParallax.borderRadius -
                            data.previewBlockParallax.scrolledPercent
                        }% ${
                            data.previewBlockParallax.borderRadius -
                            data.previewBlockParallax.scrolledPercent
                        }% 0% 0%`;
                    }
                }

                data.previewImage.style.transform =
                    "translateY(" +
                    (data.previewBlockParallax.previewImagePos +
                        data.previewBlockParallax.currentPos * 0.4) +
                    "px)";

                data.previewSecondSmallImage.style.transform =
                    "translateY(-" +
                    data.previewBlockParallax.currentPos * 0.3 +
                    "px)";

                data.previewThirdSmallImage.style.transform =
                    "translateY(-" +
                    data.previewBlockParallax.currentPos * 0.3 +
                    "px)";

                if (data.previewBlockParallax.scrolledPercent <= 0) {
                    data.previewSection.classList.remove("scroll");
                }
            }

            if (data.teamLineUpParallax.isIntersecting) {
                data.teamLineUpParallax.currentPos =
                    data.teamLineUpParallax.currentPos -
                    (prevPosition - window.scrollY);

                if (data.teamLineUpParallax.currentPos >= 0) {
                    data.teamLineUpBlock.style.transform =
                        "translateY(" +
                        data.teamLineUpParallax.currentPos * 0.2 +
                        "px)";

                    data.teamSmallImage.style.transform =
                        "translateY(-" +
                        data.teamLineUpParallax.currentPos * 0.4 +
                        "px)";
                }
            }

            if (data.solutionBlockParallax.isIntersecting) {
                data.solutionBlockParallax.currentPos =
                    data.solutionBlockParallax.currentPos -
                    (prevPosition - window.scrollY);

                if (data.solutionBlockParallax.currentPos >= 0) {
                    data.solutionBlock.style.transform =
                        "translateY(" +
                        data.solutionBlockParallax.currentPos * 0.4 +
                        "px)";
                    data.solutionSmallImage.style.transform =
                        "translateY(-" +
                        data.solutionBlockParallax.currentPos * 0.4 +
                        "px)";
                }
            }

            if (data.expertCallBlockParallax.isIntersecting) {
                data.expertCallBlockParallax.currentPos =
                    data.expertCallBlockParallax.currentPos -
                    (prevPosition - window.scrollY);

                if (data.expertCallBlockParallax.currentPos >= 0) {
                    data.expertCallBlock.style.transform =
                        "translateY(" +
                        data.expertCallBlockParallax.currentPos * 0.2 +
                        "px)";
                }
            }

            if (data.consultationBlockParallax.isIntersecting) {
                data.consultationBlockParallax.currentPos =
                    data.consultationBlockParallax.currentPos -
                    (prevPosition - window.scrollY);

                if (data.consultationBlockParallax.currentPos >= 0) {
                    data.consultationBlock.style.transform =
                        "translateY(" +
                        data.consultationBlockParallax.currentPos * 0.2 +
                        "px)";
                }
            }
        }
    };

    const accordionContentOpen = (item, elem) => {
        item.classList.toggle("open");

        if (elem.style.maxHeight) {
            elem.style.maxHeight = null;
        } else {
            elem.style.maxHeight = elem.scrollHeight + "px";
        }
    };

    // Отправка формы
    const sendForm = async (formData) => {
        try {
            //запрос поиска
            console.log("Отправка запроса:", JSON.stringify(formData));

            const response = await fetch("https://...", {
                method: "POST",
                body: JSON.stringify(formData),
            });

            if (response.ok) {
                const result = await response.json();
                console.log("Server Response:", result);
            } else {
                console.error(`Ошибка HTTP: ${response.status}`);
            }
        } catch (error) {
            console.error("Ошибка при запросе:", error);
        }
    };

    data.teamLineUp.forEach((item, i) => {
        if (item !== null) {
            const teamItemBtnOpen = item.querySelector(".team-item__header");
            const teamItemList = item.querySelector(".team-list");

            // if (i === 0) {
            //     accordionContentOpen(item, teamItemList);
            // }

            if (teamItemBtnOpen !== null) {
                teamItemBtnOpen.addEventListener("click", () => {
                    accordionContentOpen(item, teamItemList);
                });
            }
        }
    });

    data.servicesList.forEach((item, i) => {
        if (item !== null) {
            const servicesItemBtnOpen = item.querySelector(
                ".services-item__header"
            );
            const servicesItemDesc = item.querySelector(".services-item__desc");

            // if (i === 0) {
            //     accordionContentOpen(item, servicesItemDesc);
            // }

            if (servicesItemBtnOpen !== null) {
                servicesItemBtnOpen.addEventListener("click", () => {
                    accordionContentOpen(item, servicesItemDesc);
                });
            }
        }
    });

    data.priceDescElem.forEach((item, i) => {
        if (item !== null) {
            const priceItemBtnOpen = item.querySelector(".price-desc__title");
            const priceItemDesc = item.querySelector(".price-desc__elem-box");

            // if (i === 0) {
            //     accordionContentOpen(item, priceItemDesc);
            // }

            if (priceItemBtnOpen !== null) {
                priceItemBtnOpen.addEventListener("click", () => {
                    accordionContentOpen(item, priceItemDesc);
                });
            }
        }
    });

    if (data.reasonsNote !== null) {
        data.reasonsNote.addEventListener("mouseenter", () => {
            data.reasonsTooltip.classList.add("show");
        });

        data.reasonsNote.addEventListener("mouseleave", () => {
            data.reasonsTooltip.classList.remove("show");
        });
    }

    data.callbackBtn.forEach((btn) => {
        if (btn !== null) {
            btn.addEventListener("click", () => {
                data.callbackModal.classList.add("show");
                bodyScrollHide();
            });
        }
    });

    data.callbackModalBtnClose.forEach((btn) => {
        if (btn !== null) {
            btn.addEventListener("click", () => {
                data.callbackModal.classList.remove("show");
                data.callbackSuccess.classList.remove("show");
                data.callbackFormWrap.classList.remove("hidden");
                bodyScrollVisible();
            });
        }
    });

    data.callbackForm.forEach((form) => {
        const nameInput = form.querySelector('input[name="Имя"]');
        const phoneInput = form.querySelector('input[name="Телефон"]');
        const selectBlock = form.querySelector(".callback-form__select");
        const optionsList = form.querySelectorAll(".callback-form__option");
        const currentOption = form.querySelector(
            ".callback-form__select-current"
        );
        const callbackFormSelectBtn = form.querySelector(
            ".callback-form__select-header"
        );
        const sumbitBtnWrapper = form.querySelector(".section-link");

        let isFormValid = false;

        const selectData = {
            defaultTitle: "Выберите тип услуги",
            currentOptionTitle: "Выберите тип услуги",
            isSelectOpen: false,
        };

        // Валидация формы
        const validateForm = () => {
            if (!nameInput.value.trim() || !phoneInput.value.trim()) {
                if (!nameInput.value.trim() && !phoneInput.value.trim()) {
                    nameInput.parentElement.classList.add("error");
                    phoneInput.parentElement.classList.add("error");
                } else if (!nameInput.value.trim() && phoneInput.value.trim()) {
                    nameInput.parentElement.classList.add("error");
                    phoneInput.parentElement.classList.remove("error");
                } else if (nameInput.value.trim() && !phoneInput.value.trim()) {
                    nameInput.parentElement.classList.remove("error");
                    phoneInput.parentElement.classList.add("error");
                }

                sumbitBtnWrapper.classList.add("disabled");
                isFormValid = false;
            } else {
                nameInput.parentElement.classList.remove("error");
                phoneInput.parentElement.classList.remove("error");
                sumbitBtnWrapper.classList.remove("disabled");
                isFormValid = true;
            }
        };

        nameInput.addEventListener("input", () => {
            validateForm();
        });

        phoneInput.addEventListener("input", () => {
            validateForm();
        });

        callbackFormSelectBtn.addEventListener("click", () => {
            if (!selectData.isSelectOpen) {
                selectBlock.classList.add("active");
                selectData.isSelectOpen = true;
                currentOption.innerHTML = selectData.defaultTitle;
            } else {
                selectBlock.classList.remove("active");
                selectData.isSelectOpen = false;
                currentOption.innerHTML = selectData.currentOptionTitle;
            }
        });

        optionsList.forEach((option) => {
            const input = option.querySelector(".callback-form__option-input");

            option.addEventListener("click", () => {
                selectBlock.classList.remove("active");
                input.checked = true;
                selectData.currentOptionTitle = input.value;
                currentOption.innerHTML = selectData.currentOptionTitle;
                selectData.isSelectOpen = false;
            });
        });

        form.addEventListener("submit", (e) => {
            e.preventDefault();

            if (isFormValid) {
                selectBlock.classList.remove("active");
                sumbitBtnWrapper.classList.add("disabled");
                currentOption.innerHTML = selectData.defaultTitle;
                selectData.currentOptionTitle = selectData.defaultTitle;
                selectData.isSelectOpen = false;
                data.callbackModal.classList.add("show");
                data.callbackSuccess.classList.add("show");
                data.callbackFormWrap.classList.add("hidden");
                // Собираем данные формы
                const formData = new FormData(form);
                const formEntries = Object.fromEntries(formData);
                sendForm(formEntries);
                form.reset();
                isFormValid = false;
            } else {
                validateForm();
            }
        });
    });

    // Слайдер кейсов
    data.casesSlideList.forEach((slide, i) => {
        if (slide !== null) {
            const btnPrev = slide.querySelector(".cases-nav__btn-prev");
            const btnNext = slide.querySelector(".cases-nav__btn-next");

            btnPrev.addEventListener("click", () => {
                if (data.currentCasesSlide !== 0) {
                    data.currentCasesSlide--;
                    slide.classList.remove("show");
                    data.casesSlideList[data.currentCasesSlide].classList.add(
                        "show"
                    );
                }
            });

            btnNext.addEventListener("click", () => {
                if (i + 1 < data.casesSlideList.length) {
                    data.currentCasesSlide++;
                    slide.classList.remove("show");
                    data.casesSlideList[data.currentCasesSlide].classList.add(
                        "show"
                    );
                }
            });
        }
    });

    // Устанавливаем кнопке "Previous" первого слайда класс "disabled"
    if (data.swiperBtnPrevList[0] !== undefined) {
        data.swiperBtnPrevList[0].classList.add("disabled");
    }

    // Устанавливаем кнопке "Next" последнего слайда класс "disabled"
    if (
        data.swiperBtnNextList[data.swiperBtnNextList.length - 1] !== undefined
    ) {
        data.swiperBtnNextList[data.swiperBtnNextList.length - 1].classList.add(
            "disabled"
        );
    }

    // Устанавливаем кол-во слайдов
    data.totalSlideList.forEach((item) => {
        if (item !== null) {
            item.innerHTML = data.totalSlideList.length;
        }
    });

    // Устанавливаем текущий слайд
    data.currentSlideList.forEach((item, i) => {
        if (item !== null) {
            item.innerHTML = i + 1;
        }
    });

    // Создаем пагинацию свайпера
    data.swiperPagination.forEach((item, idx) => {
        if (item !== null) {
            for (let i = 0; i < data.casesSlideList.length; i++) {
                const paginationItem = document.createElement("span");
                paginationItem.classList.add("cases-img__pagination-item");
                if (i === idx) {
                    paginationItem.classList.add("active");
                }
                item.appendChild(paginationItem);
            }
        }
    });

    // Высота логотипа на сраницы 404 (чтобы футер был прижат к краю экрана)
    if (data.notFoundKeyLogo !== null) {
        data.notFoundKeyLogo.style.height = `calc(100vh - ${data.footerHeight}px - ${data.headerHeight}px - 2px)`;
    }

    const modalCallBackClose = () => {
        data.callbackModal.classList.remove("show");
        data.callbackSuccess.classList.remove("show");
        data.callbackFormWrap.classList.remove("hidden");
        bodyScrollVisible();
    };

    data.callbackModalWrap.addEventListener("click", (e) => {
        e.stopPropagation();
    });

    data.callbackModalBtnClose.forEach((btn) => {
        btn.addEventListener("click", () => {
            modalCallBackClose();
        });
    });

    data.callbackModal.addEventListener("click", () => {
        modalCallBackClose();
    });

    data.agreementLinkList.forEach((link) => {
        if (link !== null) {
            link.addEventListener("click", () => {
                data.agreementModal.classList.add("show");
                bodyScrollHide();
            });
        }
    });

    data.agreementWrapper.addEventListener("click", (e) => {
        e.stopPropagation();
    });

    data.agreementBtnClose.addEventListener("click", () => {
        data.agreementModal.classList.remove("show");
        bodyScrollVisible();
    });

    data.agreementModal.addEventListener("click", () => {
        data.agreementModal.classList.remove("show");
        bodyScrollVisible();
    });

    data.menuBtn.forEach((btn) => {
        if (btn !== null) {
            btn.addEventListener("click", () => {
                data.menuBlock.classList.toggle("show");

                if (data.menuBlock.classList.contains("show")) {
                    bodyScrollHide();
                } else {
                    bodyScrollVisible();
                }
            });
        }
    });

    // проверка установлены ли cookie
    if (!document.cookie) {
        data.cookieBlock.classList.add("show");
    }

    // Устанавливаем cookies
    const setCookie = (name, value, days) => {
        let expires = "";
        if (days) {
            let date = new Date();
            date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";

        data.cookieBlock.classList.remove("show");
    };

    data.cookieAcceptBtn.addEventListener("click", () => {
        // Устанавливаем cookie с именем "username" и значением "value" на 30 дней
        setCookie("username", "value", 30);
    });

    // Функция, которая прокручивает страницу в начало
    if (data.upBtn !== null) {
        data.upBtn.addEventListener("click", () => {
            window.scrollTo({
                top: 0,
            });
        });
    }

    // Определение видимости блоков
    const teamImageBlockObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
            // Если элемент видимый
            if (entry.isIntersecting) {
                data.teamLineUpParallax.isIntersecting = true;
            } else {
                data.teamLineUpParallax.isIntersecting = false;
            }
        });
    });

    const solutionBlockObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
            // Если элемент видимый
            if (entry.isIntersecting) {
                data.solutionBlockParallax.isIntersecting = true;
            } else {
                data.solutionBlockParallax.isIntersecting = false;
            }
        });
    });

    const expertCallBlockObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
            // Если элемент видимый
            if (entry.isIntersecting) {
                data.expertCallBlockParallax.isIntersecting = true;
            } else {
                data.expertCallBlockParallax.isIntersecting = false;
            }
        });
    });

    const consultationBlockObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
            // Если элемент видимый
            if (entry.isIntersecting) {
                data.consultationBlockParallax.isIntersecting = true;
            } else {
                data.consultationBlockParallax.isIntersecting = false;
            }
        });
    });

    const previewBlockObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
            // Если элемент видимый
            if (entry.isIntersecting) {
                data.previewBlockParallax.isIntersecting = true;
            } else {
                data.previewBlockParallax.isIntersecting = false;
            }
        });
    });

    // Начинаем отслеживать блок
    if (data.previewImageBlock !== null) {
        previewBlockObserver.observe(data.previewImageBlock);
    }

    if (data.teamImage !== null) {
        teamImageBlockObserver.observe(data.teamImage);
    }

    if (data.solutionImage !== null) {
        solutionBlockObserver.observe(data.solutionImage);
    }

    if (data.expertCallImage !== undefined) {
        expertCallBlockObserver.observe(data.expertCallImage);
    }

    if (data.consultationImage !== undefined) {
        consultationBlockObserver.observe(data.consultationImage);
    }

    window.addEventListener("scroll", () => {
        // Устанавливаем значение предыдущей позиции скрола
        const prevPosition = data.scrollPosition;
        // Устанавливаем текущую позицию скролла
        data.scrollPosition = document.documentElement.scrollTop;

        headerHidding();
        // Вызываем функцию определяющую направление скролла
        scrollDirection(prevPosition);
    });
});
