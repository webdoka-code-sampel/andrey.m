"use strict";

document.addEventListener("DOMContentLoaded", () => {
    function headerActions() {
        const header = document.querySelector(".header");

        if (header !== null) {
            const headerWrapper = header.querySelector(".header__wrap");
            const navbarList = header.querySelectorAll(".header-navbar__item");
            const headerMenu = header.querySelector(".header-menu");
            const menuBtn = headerMenu.querySelectorAll(".header-menu__btn");
            const menuBlock = headerMenu.querySelector(".menu-block");
            const underLogoText = header.querySelector(".header-logo__text");

            // Вычисляем ширину скроллбара
            const scrollbarWidth =
                window.innerWidth - document.documentElement.clientWidth;

            let menuIsActive = false;

            window.addEventListener("scroll", () => {
                let windowScrollValue = window.scrollY;

                if (windowScrollValue > 0) {
                    underLogoText.classList.add("hidden");
                    // headerWrapper.classList.add("scrolled")
                } else {
                    underLogoText.classList.remove("hidden");
                    // headerWrapper.classList.remove("scrolled")
                }
            });

            navbarList.forEach((item) => {
                if (item !== null) {
                    item.addEventListener("mouseenter", () => {
                        item.classList.toggle("hover");
                    });

                    item.addEventListener("mouseleave", () => {
                        item.classList.toggle("hover");
                    });
                }
            });

            function menuSwitcher() {
                if (!menuIsActive) {
                    menuBlock.classList.add("show");

                    setTimeout(() => {
                        document.body.style.overflow = "hidden";
                        document.body.style.marginRight = scrollbarWidth + "px";
                        menuBlock.style.paddingRight = scrollbarWidth + "px";
                    }, 300);
                    menuIsActive = true;
                } else {
                    menuBlock.classList.remove("show");
                    document.body.style.overflow = null;
                    document.body.style.marginRight = null;
                    menuBlock.style.paddingRight = null;
                    menuIsActive = false;
                }
            }

            menuBtn.forEach((btn) => {
                btn.addEventListener("click", () => {
                    menuSwitcher();
                });
            });
        }
    }

    headerActions();

    function legalServicesParallax() {
        const legalServicesBlock = document.querySelector(".legal-services");

        if (legalServicesBlock !== null) {
            const legalServicesBackground = legalServicesBlock.querySelector(
                ".legal-services__bg"
            );

            window.addEventListener("scroll", () => {
                let windowScrollValue = window.scrollY;

                legalServicesBackground.style.marginTop =
                    windowScrollValue * 0.5 + "px";
            });
        }
    }

    legalServicesParallax();

    function cookieActions() {
        const cookieBlock = document.querySelector(".cookie-block");

        if (cookieBlock !== null) {
            const cookieAcceptBtn = cookieBlock.querySelector(
                ".cookie-block__accept"
            );

            // проверка установлены ли cookie
            if (!document.cookie) {
                cookieBlock.classList.add("show");
            }

            function setCookie(name, value, days) {
                let expires = "";
                if (days) {
                    let date = new Date();
                    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie =
                    name + "=" + (value || "") + expires + "; path=/";

                cookieBlock.classList.remove("show");
            }

            cookieAcceptBtn.addEventListener("click", () => {
                // Устанавливаем cookie с именем "username" и значением "value" на 30 дней
                setCookie("username", "value", 30);
            });
        }
    }

    cookieActions();

    function callbackBlockActions() {
        const callbackBlock = document.querySelectorAll(".callback-block");
        const callbackSuccess = document.querySelector(".callback-success");
        const nameRegex = /[А-Яа-яЁё]+([-'][А-Яа-яЁё]+)?/;
        const phoneRegex = /^\+7(\s*\d){10}$/;

        // Вычисляем ширину скроллбара
        const scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;

        callbackBlock.forEach((block) => {
            if (block !== null) {
                const callbackSelect = block.querySelector(
                    ".callback-block__select"
                );
                const nameInputWrap = block.querySelectorAll(
                    ".callback-block__item"
                )[0];
                const nameInput = nameInputWrap.querySelector(
                    ".callback-block__input"
                );

                const phoneInputWrap = block.querySelectorAll(
                    ".callback-block__item"
                )[1];
                const phoneInput = phoneInputWrap.querySelector(
                    ".callback-block__input"
                );
                const submitBtn = block.querySelector(
                    ".callback-block__submit-btn"
                );

                function verificationForm() {
                    if (
                        nameRegex.test(nameInput.value) &&
                        phoneRegex.test(phoneInput.value)
                    ) {
                        nameInputWrap.classList.remove("error");
                        phoneInputWrap.classList.remove("error");
                        submitBtn.removeAttribute("disabled");
                    } else if (
                        !nameRegex.test(nameInput.value) &&
                        !phoneRegex.test(phoneInput.value)
                    ) {
                        nameInputWrap.classList.add("error");
                        phoneInputWrap.classList.add("error");

                        if (!submitBtn.hasAttribute("disabled")) {
                            submitBtn.setAttribute("disabled", "disabled");
                        }
                    } else if (
                        !nameRegex.test(nameInput.value) &&
                        phoneRegex.test(phoneInput.value)
                    ) {
                        nameInputWrap.classList.add("error");
                        phoneInputWrap.classList.remove("error");

                        if (!submitBtn.hasAttribute("disabled")) {
                            submitBtn.setAttribute("disabled", "disabled");
                        }
                    } else if (
                        nameRegex.test(nameInput.value) &&
                        !phoneRegex.test(phoneInput.value)
                    ) {
                        nameInputWrap.classList.remove("error");
                        phoneInputWrap.classList.add("error");

                        if (!submitBtn.hasAttribute("disabled")) {
                            submitBtn.setAttribute("disabled", "disabled");
                        }
                    }
                }

                nameInput.addEventListener("input", () => {
                    verificationForm();
                });

                phoneInput.addEventListener("input", () => {
                    verificationForm();
                });

                if (callbackSelect !== null) {
                    const callbackSelectBtn = callbackSelect.querySelector(
                        ".callback-block__select-btn"
                    );
                    const callbackOptionsItem = callbackSelect.querySelectorAll(
                        ".callback-block__options-item"
                    );
                    const currentOption = callbackSelect.querySelector(
                        ".callback-block__select-current"
                    );

                    const optionsData = {
                        isOpen: false,
                        selectTitle: "Выберите вашу роль в сделке",
                        currentOptionValue: null,
                    };

                    function optionsHandler(option) {
                        if (option === undefined) {
                            if (!optionsData.isOpen) {
                                optionsData.isOpen = true;
                                callbackSelect.classList.add("active");
                                currentOption.innerHTML =
                                    optionsData.selectTitle;
                                currentOption.classList.remove("selected");
                            } else {
                                optionsData.isOpen = false;
                                callbackSelect.classList.remove("active");
                                if (optionsData.currentOptionValue !== null) {
                                    currentOption.innerHTML =
                                        optionsData.currentOptionValue;
                                    currentOption.classList.add("selected");
                                }
                            }
                        } else {
                            const optionValue = option.querySelector(
                                ".callback-block__options-radio"
                            );

                            optionsData.currentOptionValue = optionValue.value;
                            currentOption.innerHTML =
                                optionsData.currentOptionValue;
                            optionValue.checked = true;
                            optionsData.isOpen = false;
                            currentOption.classList.add("selected");
                            callbackSelect.classList.remove("active");
                        }
                    }

                    callbackSelectBtn.addEventListener("click", () => {
                        optionsHandler();
                    });

                    callbackOptionsItem.forEach((option) => {
                        option.addEventListener("click", () => {
                            optionsHandler(option);
                        });
                    });
                }

                submitBtn.addEventListener("click", (e) => {
                    e.preventDefault();
                    callbackSuccess.classList.add("show");
                    document
                        .querySelector(".modal-callback")
                        .classList.remove("show");
                    document.body.style.overflow = "hidden";
                    document.body.style.marginRight = scrollbarWidth + "px";
                    document.querySelector(".header").style.paddingRight =
                        scrollbarWidth + "px";
                    nameInput.value = "";
                    phoneInput.value = "";
                });
            }
        });
    }

    callbackBlockActions();

    function callbackModalActions() {
        const header = document.querySelector(".header");
        const callbackModal = document.querySelector(".modal-callback");
        const callbackBtnOpen = document.querySelectorAll(".callback-btn");
        const callbackSuccess = document.querySelector(".callback-success");
        const agreementModal = document.querySelector(".agreement");
        const agreementCloseBtn = document.querySelector(".agreement__close");

        // проверка установлены ли cookie
        if (!document.cookie) {
            setTimeout(() => {
                agreementModal.classList.add("show");
                document.body.style.overflow = "hidden";
                document.body.style.marginRight = scrollbarWidth + "px";
                header.style.paddingRight = scrollbarWidth + "px";
            }, 3000);
        }

        // Вычисляем ширину скроллбара
        const scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;

        callbackBtnOpen.forEach((btn) => {
            if (btn !== null) {
                btn.addEventListener("click", () => {
                    callbackModal.classList.add("show");
                    document.body.style.overflow = "hidden";
                    document.body.style.marginRight = scrollbarWidth + "px";
                    header.style.paddingRight = scrollbarWidth + "px";
                });
            }
        });

        function callbackModalClose() {
            callbackModal.classList.remove("show");
            document.body.style.overflow = null;
            document.body.style.marginRight = null;
            header.style.paddingRight = null;
        }

        if (callbackModal !== null) {
            const callbackBtnclose = callbackModal.querySelector(
                ".callback-block__close"
            );

            callbackBtnclose.addEventListener("click", () => {
                callbackModalClose();
            });
        }

        document.addEventListener("click", (e) => {
            if (e.target.classList.contains("modal-callback__wrap")) {
                callbackModalClose();
            }
        });

        function callbackSuccessClose() {
            callbackSuccess.classList.remove("show");
            document.body.style.overflow = null;
            document.body.style.marginRight = null;
            header.style.paddingRight = null;
        }

        if (callbackSuccess !== null) {
            const callbackSuccessBtnclose = callbackSuccess.querySelector(
                ".callback-success__close"
            );
            callbackSuccessBtnclose.addEventListener("click", () => {
                callbackSuccessClose();
            });
        }

        document.addEventListener("click", (e) => {
            if (e.target.classList.contains("callback-success__wrap")) {
                callbackSuccessClose();
            }
        });

        function agreementModalClose() {
            agreementModal.classList.remove("show");
            document.body.style.overflow = null;
            document.body.style.marginRight = null;
            header.style.paddingRight = null;
        }

        if (agreementCloseBtn !== null) {
            agreementCloseBtn.addEventListener("click", () => {
                agreementModalClose();
            });
        }

        document.addEventListener("click", (e) => {
            if (e.target.classList.contains("agreement__wrap")) {
                agreementModalClose();
            }
        });
    }

    callbackModalActions();

    function aboutCompanyAnimation() {
        const aboutCompanyBlock = document.querySelector(".about-company");

        if (aboutCompanyBlock !== null) {
            const numbersElem = aboutCompanyBlock.querySelectorAll(
                ".about-company__num"
            );
            const aboutCompanyInfo = aboutCompanyBlock.querySelector(
                ".about-company__info"
            );
            const cards = aboutCompanyBlock.querySelectorAll(
                ".about-company__card"
            );

            function animations() {
                aboutCompanyInfo.classList.add("show");
                cards[0].classList.add("show");
                if (window.innerWidth < 1440) {
                    numbersElem[3].classList.add("show");
                } else {
                    numbersElem[0].classList.add("show");
                }

                let index = 1;
                const interval = setInterval(() => {
                    // Добавляем класс "show" к текущему элементу
                    cards[index].classList.add("show");

                    if (window.innerWidth < 1440) {
                        numbersElem[index + 3].classList.add("show");
                    } else {
                        numbersElem[index].classList.add("show");
                    }

                    index++;

                    // Если мы достигли конца списка элементов, очищаем интервал
                    if (index === cards.length) {
                        clearInterval(interval);
                    }
                }, 1000); // Интервал в миллисекундах (1 секунда = 1000 миллисекунд)
            }

            function handleIntersection(entries, observer) {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        // Запускаем анимацию
                        animations();
                        // Добавляем класс "visible", когда блок виден
                        entry.target.classList.add("visible");
                        // Прекращаем отслеживать этот блок после добавления класса
                        observer.unobserve(entry.target);
                    }
                });
            }

            // Создаем новый экземпляр Intersection Observer
            let observer = new IntersectionObserver(handleIntersection, {
                // пороговое значение, чтобы определить, насколько должен быть видим блок
                threshold: 0.5,
            });

            // Начинаем отслеживать блок
            observer.observe(aboutCompanyBlock);
        }
    }

    aboutCompanyAnimation();

    function expertiseBlockActions() {
        const expertiseBlock = document.querySelector(".expertise");

        if (expertiseBlock !== null) {
            const typeBtns = expertiseBlock.querySelectorAll(
                ".expertise-preview__type-title"
            );
            const typeImgBlocks = expertiseBlock.querySelectorAll(
                ".expertise-preview__img-block"
            );
            const typeOfTabsBlocks =
                expertiseBlock.querySelectorAll(".expertise-tabs");

            let currentType = 0;

            typeBtns.forEach((btn, i) => {
                const typeElem = btn.parentElement;

                btn.addEventListener("click", () => {
                    typeBtns.forEach((item) => {
                        const typeElem = item.parentElement;
                        typeElem.classList.remove("active");
                    });
                    typeImgBlocks[currentType].classList.remove("active");
                    typeOfTabsBlocks[currentType].classList.remove("active");

                    currentType = i;

                    typeElem.classList.add("active");
                    typeImgBlocks[currentType].classList.add("active");
                    typeOfTabsBlocks[currentType].classList.add("active");
                });
            });

            typeOfTabsBlocks.forEach((type, typeIdx) => {
                const tabsBlocks = type.querySelectorAll(
                    ".expertise-tabs__list-wrap"
                );
                const typeImgBlock = typeImgBlocks[typeIdx];
                const tabBtns = type.querySelectorAll(
                    ".expertise-tabs__btn-text"
                );

                tabBtns.forEach((btn, idx) => {
                    const tabBtnsWrapper = btn.closest(
                        ".expertise-tabs__header"
                    );
                    const btns = tabBtnsWrapper.querySelectorAll(
                        ".expertise-tabs__btn"
                    );
                    const imgLists = typeImgBlock.querySelectorAll(
                        ".expertise-preview__img-list"
                    );

                    btn.addEventListener("click", () => {
                        tabsBlocks.forEach((tab) => {
                            tab.classList.remove("active");
                        });
                        tabsBlocks[idx].classList.add("active");

                        btns.forEach((el) => {
                            el.classList.remove("active");
                        });
                        btns[idx].classList.add("active");

                        imgLists.forEach((list) => {
                            list.classList.remove("active");
                        });
                        imgLists[idx].classList.add("active");
                    });
                });

                tabsBlocks.forEach((block, blockIdx) => {
                    const tabsItem = block.querySelectorAll(
                        ".expertise-tabs__item"
                    );
                    const imgWrapper = typeImgBlock.querySelectorAll(
                        ".expertise-preview__img-list"
                    )[blockIdx];

                    tabsItem.forEach((item, itemIdx) => {
                        const tabList = item.parentElement;
                        const items = tabList.querySelectorAll(
                            ".expertise-tabs__item"
                        );
                        const images = imgWrapper.querySelectorAll(
                            ".expertise-preview__img"
                        );

                        item.addEventListener("click", () => {
                            // Смена активного пункта в табе
                            items.forEach((elem) => {
                                elem.classList.remove("active");
                            });
                            item.classList.add("active");

                            // Смена изображения соответствующему активному пункту в табе
                            images.forEach((img) => {
                                img.classList.remove("active");
                            });
                            images[itemIdx].classList.add("active");
                        });
                    });
                });
            });
        }
    }

    expertiseBlockActions();

    function verificationBlockActions() {
        const verificationBoxes = document.querySelectorAll(
            ".verification__desc-box"
        );

        verificationBoxes.forEach((box) => {
            if (box !== null) {
                const btnOpen = box.querySelector(".verification__desc-open");
                const verificationList = box.querySelector(
                    ".verification__desc-list"
                );
                const descTitleBtnOpen = box.querySelector(
                    ".verification__desc-title"
                );

                function contentOpen() {
                    box.classList.toggle("active");
                    if (verificationList.style.maxHeight) {
                        verificationList.style.maxHeight = null;
                    } else {
                        verificationList.style.maxHeight =
                            verificationList.scrollHeight + "px";
                    }
                }

                btnOpen.addEventListener("click", () => {
                    contentOpen();
                });

                descTitleBtnOpen.addEventListener("click", () => {
                    contentOpen();
                });
            }
        });
    }

    verificationBlockActions();

    function legalConsultationDescActions() {
        const descriptionBoxes = document.querySelectorAll(
            ".legal-consultation__desc-box"
        );

        descriptionBoxes.forEach((box) => {
            if (box !== null) {
                const btnOpen = box.querySelector(
                    ".legal-consultation__desc-open"
                );
                const descriptionList = box.querySelector(
                    ".legal-consultation__desc-list"
                );
                const descTopBtnOpen = box.querySelector(
                    ".legal-consultation__desc-top"
                );

                function contentOpen() {
                    box.classList.toggle("active");
                    if (descriptionList.style.maxHeight) {
                        descriptionList.style.maxHeight = null;
                    } else {
                        descriptionList.style.maxHeight =
                            descriptionList.scrollHeight + "px";
                    }
                }

                btnOpen.addEventListener("click", () => {
                    contentOpen();
                });

                descTopBtnOpen.addEventListener("click", () => {
                    contentOpen();
                });
            }
        });
    }

    legalConsultationDescActions();

    function companyLogoParallax() {
        const companyLogoBlock = document.querySelector(".company-logo");

        if (companyLogoBlock !== null) {
            const blockBackground =
                companyLogoBlock.querySelector(".company-logo__img");
            const keyBackground = companyLogoBlock.querySelector(
                ".company-logo__key-bg"
            );

            let isIntersecting = false;
            let entryPos = null;
            let currentPos = null;

            function blockScroll() {
                let lastScrollTop = 0;

                window.addEventListener("scroll", () => {
                    if (isIntersecting) {
                        let scrollTop = window.scrollY;

                        if (scrollTop > lastScrollTop) {
                            currentPos = window.scrollY - entryPos;
                        } else {
                            currentPos = window.scrollY - entryPos;
                        }

                        lastScrollTop = scrollTop <= 0 ? 0 : scrollTop;

                        blockBackground.style.transform =
                            "translateY(" + currentPos * 0.5 + "px)";
                        keyBackground.setAttribute(
                            "y",
                            -600 - currentPos * 0.3
                        );
                    }
                });
            }

            const observer = new IntersectionObserver((entries) => {
                entries.forEach((entry) => {
                    // Если элемент видимый
                    if (entry.isIntersecting) {
                        entryPos = window.scrollY;

                        isIntersecting = true;
                    } else {
                        isIntersecting = false;
                    }
                    blockScroll();
                });
            });

            // Начинаем отслеживать блок
            observer.observe(companyLogoBlock);
        }
    }

    companyLogoParallax();

    // Функция, которая прокручивает страницу в начало
    function scrollToTop() {
        const upBtn = document.querySelector(".footer__btn-up");

        if (upBtn !== null) {
            upBtn.addEventListener("click", () => {
                window.scrollTo({
                    top: 0,
                });
            });
        }
    }

    scrollToTop();

    if (document.querySelector(".services-swiper") !== null) {
        const servicesSwiper = new Swiper(".services-swiper", {
            slidesPerView: 1,
            speed: 1000,
            spaceBetween: 16,
            grabCursor: true,

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1.2,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 1.7,
                },
                // when window width is >= 1440px
                1440: {
                    slidesPerView: 3.1,
                },
                // when window width is >= 1920px
                1920: {
                    slidesPerView: 3.5,
                },
            },
        });
    }

    if (document.querySelector(".consultation-swiper") !== null) {
        const consultationSwiper = new Swiper(".consultation-swiper", {
            slidesPerView: 1,
            speed: 1000,
            spaceBetween: 16,
            grabCursor: true,

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1.2,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 1.9,
                },
                // when window width is >= 1440px
                1440: {
                    slidesPerView: 3.1,
                },
                // when window width is >= 1920px
                1920: {
                    slidesPerView: 3.7,
                },
            },
        });
    }

    if (document.querySelector(".order-swiper") !== null) {
        const orderSwiper = new Swiper(".order-swiper", {
            slidesPerView: 1,
            speed: 1000,
            spaceBetween: 16,
            grabCursor: true,

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1.3,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 1.7,
                },
                // when window width is >= 1440px
                1440: {
                    slidesPerView: 3.1,
                },
                // when window width is >= 1920px
                1920: {
                    slidesPerView: 3.7,
                },
            },
        });
    }

    if (document.querySelector(".team-swiper") !== null) {
        const teamSwiper = new Swiper(".team-swiper", {
            slidesPerView: 1,
            speed: 1000,
            spaceBetween: 20,
            grabCursor: true,

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1.6,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 2.5,
                },
                // when window width is >= 1440px
                1440: {
                    slidesPerView: 4.5,
                },
                // when window width is >= 1920px
                1920: {
                    slidesPerView: 4.8,
                },
            },
        });
    }

    if (document.querySelector(".feedback-swiper") !== null) {
        const feedbackSwiper = new Swiper(".feedback-swiper", {
            slidesPerView: 1,
            speed: 1000,
            spaceBetween: 20,
            grabCursor: true,

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1.2,
                    spaceBetween: 16,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 1.9,
                    spaceBetween: 16,
                },
                // when window width is >= 1440px
                1440: {
                    slidesPerView: 2.9,
                },
                // when window width is >= 1920px
                1920: {
                    slidesPerView: 3.2,
                },
            },
        });
    }

    if (document.querySelector(".about-company__swiper") !== null) {
        const aboutCompanySwiper = new Swiper(".about-company__swiper", {
            slidesPerView: 1,
            speed: 1000,
            spaceBetween: 20,
            grabCursor: true,

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1.2,
                    spaceBetween: 16,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 3,
                    spaceBetween: 16,
                },
                // when window width is >= 1440px
                1440: {
                    slidesPerView: 3,
                    spaceBetween: 16,
                },
                // when window width is >= 1920px
                1920: {
                    slidesPerView: 3,
                },
            },
        });
    }
});
