"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const data = {
        // CommonData
        body: document.body,
        header: document.querySelector(".header"),
        main: document.querySelector(".main"),
        footer: document.querySelector(".footer"),
        languageSelectBtn: document.querySelector(".language-select__btn"),
        languageSelectCurrent: document.querySelector(
            ".language-select__current"
        ),
        languageOptionsBlock: document.querySelector(".language-options"),
        languageOptionsList: document.querySelectorAll(
            ".language-options__item"
        ),
        callbackBtn: document.querySelectorAll(".callback-btn"),
        callbackModal: document.querySelector(".callback-modal"),
        callbackModalWrappper: document.querySelector(".callback-modal__wrap"),
        callbackForm: document.querySelectorAll(".callback-form"),
        callbackClose: document.querySelectorAll(".callback-form__close"),
        callbackSuccess: document.querySelector(".callback-success"),
        navbar: document.querySelector(".navbar"),
        navbarList: document.querySelectorAll(".navbar__item"),
        burgerMenu: document.querySelector(".burger-menu"),

        // Вычисляемые данные
        scrollPosition: null,
        scrollbarWidth: null,
        headerHeight: null,
        footerHeight: null,

        // Состояния
        languageOptionsShow: false,
        menuShow: false,

        // Статичные данные
        languages: {
            ru: "Ru",
            ger: "De",
            en: "En",
        },
        // Размеры экранов
        desktopSize: 1440,
        tabletSize: 1024,
        mobileSize: 768,

        // URLs
        CALLBACK_FORM_SEND_URL: "/local/ajax/formHandler.php",
    };

    const setData = () => {
        // Высота Хедера
        data.headerHeight = data.header.clientHeight;
        // Высота Футера
        data.footerHeight = data.footer.clientHeight;
        // Текущий скролл
        data.scrollPosition = document.documentElement.scrollTop;
        // Ширина скроллбара
        data.scrollbarWidth =
            window.innerWidth - document.documentElement.clientWidth;
    };

    setData();

    // События
    const bodyScrollHide = () => {
        data.body.style.overflow = "hidden";
        data.body.style.marginRight = data.scrollbarWidth + "px";
    };

    const bodyScrollVisible = () => {
        data.body.style.overflow = null;
        data.body.style.marginRight = null;
    };

    const languageOptionsClose = () => {
        data.languageOptionsBlock.classList.remove("show");
        data.languageOptionsShow = false;
    };

    const menuClose = () => {
        data.burgerMenu.classList.remove("active");
        data.navbar.classList.remove("show");
        data.menuShow = false;
    };

    if (data.languageSelectBtn !== null) {
        data.languageSelectBtn.addEventListener("click", () => {
            if (!data.languageOptionsShow) {
                data.languageOptionsBlock.classList.add("show");
                data.languageOptionsShow = true;
            } else {
                languageOptionsClose();
            }
        });
    }

    data.languageOptionsList.forEach((language, i) => {
        if (language !== null) {
            language.addEventListener("click", () => {
                languageOptionsClose();

                if (i === 0) {
                    data.languageSelectCurrent.innerText = data.languages.ru;
                }

                if (i === 1) {
                    data.languageSelectCurrent.innerText = data.languages.ger;
                }

                if (i === 2) {
                    data.languageSelectCurrent.innerText = data.languages.en;
                }
            });
        }
    });

    data.callbackBtn.forEach((btn) => {
        if (btn !== null) {
            btn.addEventListener("click", () => {
                data.callbackModal.classList.add("show");
                bodyScrollHide();
            });
        }
    });

    data.callbackClose.forEach((btn) => {
        if (btn !== null) {
            btn.addEventListener("click", () => {
                data.callbackModal.classList.remove("show");
                data.callbackForm.forEach((form) => {
                    form.classList.remove("hidden");
                });
                data.callbackSuccess.classList.remove("show");
                bodyScrollVisible();
            });
        }
    });

    if (data.callbackModalWrappper !== null) {
        data.callbackModalWrappper.addEventListener("click", () => {
            data.callbackModal.classList.remove("show");
            data.callbackForm.forEach((form) => {
                form.classList.remove("hidden");
            });
            data.callbackSuccess.classList.remove("show");
            bodyScrollVisible();
        });
    }

    if (data.burgerMenu !== null) {
        data.burgerMenu.addEventListener("click", () => {
            if (!data.menuShow) {
                data.burgerMenu.classList.add("active");
                data.navbar.classList.add("show");
                data.menuShow = true;
            } else {
                menuClose();
            }
        });
    }

    data.navbarList.forEach((item) => {
        if (item !== null) {
            item.addEventListener("click", () => {
                menuClose();
            });
        }
    });

    // Отправка формы
    const sendForm = async (formData) => {
        try {
            const response = await fetch(data.CALLBACK_FORM_SEND_URL, {
                method: "POST",
                body: formData,
            });

            if (response.ok) {
                const result = await response.json();
                console.log("Server Response:", result);
            } else {
                console.error(`Ошибка HTTP: ${response.status}`);
            }
        } catch (error) {
            console.error("Ошибка при запросе:", error);
        }
    };

    data.callbackForm.forEach((form) => {
        if (form !== null) {
            const nameInput = form.querySelector('input[name="name"]');
            const contactInput = form.querySelector('input[name="contact"]');
            const agreementCheckboxInput = form.querySelector(
                ".callback-form__check-input"
            );
            const attachmentInput = form.querySelector(
                ".callback-form__attachment-input"
            );
            const agreementCheckboxBtn = form.querySelector(
                ".callback-form__check"
            );
            const contactItemList = form.querySelectorAll(
                ".callback-form__contact-item"
            );
            const attachmentBox = form.querySelector(
                ".callback-form__attachment"
            );
            const loadBtn = form.querySelector(
                ".callback-form__attachment-btn"
            );
            const loadDelBtn = form.querySelector(
                ".callback-form__attachment-del"
            );
            const loadInput = form.querySelector(
                ".callback-form__attachment-input"
            );
            const attachmentFileWrapper = form.querySelector(
                ".callback-form__attachment-file"
            );

            let isFormValid = false;

            // Валидация формы
            const validateForm = () => {
                if (
                    !nameInput.value.trim() ||
                    !contactInput.value.trim() ||
                    !agreementCheckboxInput.checked
                ) {
                    if (
                        !nameInput.value.trim() &&
                        !contactInput.value.trim() &&
                        !agreementCheckboxInput.checked
                    ) {
                        nameInput.parentElement.classList.add("error");
                        contactInput.parentElement.classList.add("error");
                        agreementCheckboxInput.parentElement.classList.add(
                            "error"
                        );
                    } else if (
                        !nameInput.value.trim() &&
                        !contactInput.value.trim() &&
                        agreementCheckboxInput.checked
                    ) {
                        nameInput.parentElement.classList.add("error");
                        contactInput.parentElement.classList.add("error");
                        agreementCheckboxInput.parentElement.classList.remove(
                            "error"
                        );
                    } else if (
                        !nameInput.value.trim() &&
                        contactInput.value.trim() &&
                        !agreementCheckboxInput.checked
                    ) {
                        nameInput.parentElement.classList.add("error");
                        contactInput.parentElement.classList.remove("error");
                        agreementCheckboxInput.parentElement.classList.add(
                            "error"
                        );
                    } else if (
                        nameInput.value.trim() &&
                        !contactInput.value.trim() &&
                        !agreementCheckboxInput.checked
                    ) {
                        nameInput.parentElement.classList.remove("error");
                        contactInput.parentElement.classList.add("error");
                        agreementCheckboxInput.parentElement.classList.add(
                            "error"
                        );
                    } else if (
                        nameInput.value.trim() &&
                        contactInput.value.trim() &&
                        !agreementCheckboxInput.checked
                    ) {
                        nameInput.parentElement.classList.remove("error");
                        contactInput.parentElement.classList.remove("error");
                        agreementCheckboxInput.parentElement.classList.add(
                            "error"
                        );
                    } else if (
                        nameInput.value.trim() &&
                        !contactInput.value.trim() &&
                        agreementCheckboxInput.checked
                    ) {
                        nameInput.parentElement.classList.remove("error");
                        contactInput.parentElement.classList.add("error");
                        agreementCheckboxInput.parentElement.classList.remove(
                            "error"
                        );
                    } else if (
                        !nameInput.value.trim() &&
                        contactInput.value.trim() &&
                        agreementCheckboxInput.checked
                    ) {
                        nameInput.parentElement.classList.add("error");
                        contactInput.parentElement.classList.remove("error");
                        agreementCheckboxInput.parentElement.classList.remove(
                            "error"
                        );
                    }

                    // sumbitBtnWrapper.classList.add("disabled");
                    isFormValid = false;
                } else {
                    nameInput.parentElement.classList.remove("error");
                    contactInput.parentElement.classList.remove("error");
                    agreementCheckboxInput.parentElement.classList.remove(
                        "error"
                    );
                    // sumbitBtnWrapper.classList.remove("disabled");
                    isFormValid = true;
                }
            };

            contactItemList.forEach((item) => {
                const contactRadio = item.querySelector(
                    ".callback-form__contact-radio"
                );
                const contactTitle = item.querySelector(
                    ".callback-form__contact-title"
                );

                item.addEventListener("click", () => {
                    if (!contactRadio.checked) {
                        contactItemList.forEach((el) => {
                            el.classList.remove("active");
                        });

                        contactRadio.checked = true;
                        item.classList.add("active");
                        contactInput.setAttribute(
                            "placeholder",
                            contactTitle.innerHTML
                        );
                    }

                    console.log(contactInput);
                });
            });

            nameInput.addEventListener("input", () => {
                validateForm();
            });

            contactInput.addEventListener("input", () => {
                validateForm();
            });

            agreementCheckboxInput.addEventListener("change", () => {
                validateForm();
            });

            agreementCheckboxBtn.addEventListener("click", () => {
                agreementCheckboxBtn.parentElement.classList.toggle("active");

                agreementCheckboxInput.checked
                    ? (agreementCheckboxInput.checked = false)
                    : (agreementCheckboxInput.checked = true);
            });

            loadBtn.addEventListener("click", () => {
                loadInput.click();
            });

            const updateLoadList = () => {
                const files = loadInput.files;
                const file = files[0];
                const lastItem = loadDelBtn;
                attachmentFileWrapper.innerHTML = "";

                if (files.length) {
                    attachmentBox.classList.add("active");

                    const elem = document.createElement("div");
                    elem.className = "callback-form__attachment-item";
                    elem.textContent = file.name;

                    // Вставляем элемент удаления списка файлов
                    attachmentFileWrapper.appendChild(lastItem);

                    // Вставляем новый элемент перед последним
                    attachmentFileWrapper.insertBefore(elem, lastItem);
                } else {
                    attachmentBox.classList.remove("active");
                }
            };

            loadInput.addEventListener("change", () => {
                updateLoadList();
            });

            loadDelBtn.addEventListener("click", () => {
                loadInput.value = "";
                updateLoadList();
            });

            form.addEventListener("click", (e) => {
                e.stopPropagation();
            });

            form.addEventListener("submit", (e) => {
                e.preventDefault();
                validateForm();

                if (isFormValid) {
                    // Собираем данные формы
                    const formData = new FormData(form);
                    formData.append("file", attachmentInput.files[0]);
                    sendForm(formData);
                    form.reset();
                    data.callbackModal.classList.add("show");
                    data.callbackSuccess.classList.add("show");
                    data.callbackModal
                        .querySelector(".callback-form")
                        .classList.add("hidden");
                    // form.classList.add("hidden");
                    agreementCheckboxBtn.parentElement.classList.toggle(
                        "active"
                    );
                    agreementCheckboxInput.checked = false;
                    contactItemList.forEach((item, i) => {
                        const contactTitle = item.querySelector(
                            ".callback-form__contact-title"
                        );

                        item.classList.remove("active");

                        if (i === 0) {
                            item.classList.add("active");
                            contactInput.setAttribute(
                                "placeholder",
                                contactTitle.innerHTML
                            );
                        }
                    });
                    updateLoadList();
                    isFormValid = false;
                }
            });
        }
    });

    if (data.callbackSuccess !== null) {
        data.callbackSuccess.addEventListener("click", (e) => {
            e.stopPropagation();
        });
    }

    window.addEventListener("scroll", () => {});

    document.addEventListener("click", (e) => {
        if (!e.target.closest(".language-select")) {
            languageOptionsClose();
        }

        if (!e.target.closest(".navbar") && !e.target.closest(".burger-menu")) {
            menuClose();
        }
    });
});
