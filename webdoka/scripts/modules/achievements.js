"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const data = {
        yearsList: document.querySelectorAll(".filter-chips__item"),
        achievementsList: document.querySelectorAll(".achievements-list"),
    };

    const showAchievementBlock = (tabYear) => {
        data.achievementsList.forEach((item) => {
            if (item !== null) {
                const itemYear = item.dataset.year;
                item.classList.remove("active");

                if (tabYear === itemYear) {
                    item.classList.add("active");
                }
            }
        });
    };

    data.yearsList.forEach((tab) => {
        if (tab !== null) {
            const tabYear = tab.dataset.year;

            tab.addEventListener("click", () => {
                if (!tab.classList.contains("active")) {
                    tab.parentElement
                        .querySelectorAll(".filter-chips__item")
                        .forEach((el) => {
                            el.classList.remove("active");
                        });
                    tab.classList.add("active");

                    showAchievementBlock(tabYear);
                }
            });
        }
    });
});
