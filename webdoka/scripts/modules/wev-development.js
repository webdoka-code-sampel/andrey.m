"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const anotherServicesSwiper = document.querySelector(".services-swiper");

    if (anotherServicesSwiper !== null) {
        const servicesSwiper = new Swiper(anotherServicesSwiper, {
            slidesPerView: 3.2,
            speed: 1000,
            spaceBetween: 30,
            grabCursor: true,

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1.2,
                    spaceBetween: 10,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 2.2,
                },
                // when window width is >= 1024px
                1024: {
                    slidesPerView: 3.2,
                    spaceBetween: 20,
                },
                // when window width is >= 1440px
                1440: {
                    slidesPerView: 3.2,
                },
            },
        });
    }
});
