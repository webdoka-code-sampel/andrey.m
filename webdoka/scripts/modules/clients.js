"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const data = {
        clientsList: document.querySelectorAll(".clients-item"),
        filterchips: document.querySelectorAll(".filter-chips__item"),
    };

    const hideElements = (chipType) => {
        data.clientsList.forEach((item) => {
            if (item !== null) {
                const itemType = item.dataset.type;
                item.classList.remove("hidden");

                if (chipType !== "all") {
                    if (chipType !== itemType) {
                        item.classList.add("hidden");
                    }
                }
            }
        });
    };

    data.filterchips.forEach((chip) => {
        if (chip !== null) {
            const chipType = chip.dataset.type;

            chip.addEventListener("click", () => {
                if (!chip.classList.contains("active")) {
                    chip.parentElement
                        .querySelectorAll(".filter-chips__item")
                        .forEach((el) => {
                            el.classList.remove("active");
                        });
                    chip.classList.add("active");

                    hideElements(chipType);
                }
            });
        }
    });
});
