"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const data = {
        detailsItemList: document.querySelectorAll(".contacts-details__item"),
        detailsOpenBtn: document.querySelector(".contacts-details__open-btn"),
    };

    data.detailsOpenBtn.addEventListener("click", () => {
        data.detailsItemList.forEach((item) => {
            item.classList.remove("hidden");
            data.detailsOpenBtn.remove();
        });
    });
});
