"use strict";

document.addEventListener("DOMContentLoaded", () => {
    const data = {
        caseSwiper: document.querySelector(".case-swiper"),
        anotherCasesSwiper: document.querySelector(".cases-swiper"),
    };

    if (data.caseSwiper !== null) {
        const caseSwiper = new Swiper(data.caseSwiper, {
            slidesPerView: 1,
            speed: 1000,
            spaceBetween: 16,
            grabCursor: true,

            pagination: {
                el: ".swiper-pagination",
                type: "bullets",
            },

            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    }

    if (data.anotherCasesSwiper !== null) {
        const anotherCasesSwiper = new Swiper(data.anotherCasesSwiper, {
            slidesPerView: 3.2,
            speed: 1000,
            spaceBetween: 30,
            grabCursor: true,

            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1.1,
                    spaceBetween: 10,
                },
                // when window width is >= 768px
                768: {
                    slidesPerView: 1.5,
                },
                // when window width is >= 1024px
                1024: {
                    slidesPerView: 2.7,
                    spaceBetween: 20,
                },
                // when window width is >= 1440px
                1440: {
                    slidesPerView: 3.2,
                },
            },
        });
    }
});
